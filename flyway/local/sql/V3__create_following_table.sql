CREATE TABLE IF NOT EXISTS following (
  f_id          BIGINT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  u_id          BIGINT             NOT NULL,
  u_follower_id BIGINT             NOT NULL,
  FOREIGN KEY (u_id) REFERENCES user_info (u_id)
    ON DELETE CASCADE,
  FOREIGN KEY (u_follower_id) REFERENCES user_info (u_id)
    ON DELETE CASCADE
);



