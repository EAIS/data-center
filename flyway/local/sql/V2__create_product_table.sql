CREATE TABLE IF NOT EXISTS product (
  p_id             BIGINT PRIMARY KEY NOT NULL AUTO_INCREMENT, -- product id, the primary key of this table
  u_id             BIGINT NOT NULL ,
  p_title          VARCHAR(100)       NOT NULL, -- product title, using for brief description product
  p_image          VARCHAR(100) UNIQUE, -- image for present product
  p_location       VARCHAR(100)       NOT NULL, -- product location, using for use to find it
  p_price_origin   DECIMAL            NOT NULL, -- original price of this product
  p_price_discount DECIMAL            NOT NULL, -- discount price of this product
  p_valid_date     DATE               NOT NULL, -- valid date for this discount price
  p_valid          BOOLEAN            NOT NULL, -- valid flag for this product
  p_approve        BOOLEAN            NOT NULL, -- appoved by admin for this product
  p_stock          INTEGER            NOT NULL, -- rest number of this product
  deleted          BOOLEAN            NOT NULL,
  FOREIGN KEY (u_id) REFERENCES user_info (u_id)
    ON DELETE CASCADE
);







