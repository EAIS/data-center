ALTER TABLE user_info
  ADD INDEX user_id_index (u_id);
ALTER TABLE user_info
  ADD INDEX user_phone_number_index (phone_number);
ALTER TABLE user_info
  ADD INDEX user_account_index (u_account);
ALTER TABLE product
  ADD INDEX product_id_index (p_id);
ALTER TABLE following
  ADD INDEX following_id_index (f_id);
ALTER TABLE message
  ADD INDEX message_id_index (m_id);
ALTER TABLE shopping_cart
  ADD INDEX shopping_cart_id_index (s_id);

