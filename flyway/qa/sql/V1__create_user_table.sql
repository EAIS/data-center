CREATE TABLE IF NOT EXISTS user_info (
  u_id         BIGINT PRIMARY KEY NOT NULL AUTO_INCREMENT, -- user id, the primary key of this table
  phone_number VARCHAR(11) UNIQUE NOT NULL, -- user mobile phone number, using for sign up and sign in
  u_account    VARCHAR(50) UNIQUE, -- user bias, showing in the system front-end, also can using for login
  password     VARCHAR(64)        NOT NULL, -- user password for sign in
  u_name       VARCHAR(20) UNIQUE, -- user real name
  u_identity   VARCHAR(18) UNIQUE, -- user identity(shen fen zheng)
  u_address    VARCHAR(100), -- user address
  u_type       ENUM ('NORMAL', 'CUSTOMER') NOT NULL , -- user type, 1 stands for normal user and default value, 2 stands for customer
  deleted      BOOLEAN            NOT NULL
);


