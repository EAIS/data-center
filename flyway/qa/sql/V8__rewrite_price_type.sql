ALTER TABLE product
  MODIFY COLUMN p_price_origin DECIMAL(12, 2);

ALTER TABLE product
  MODIFY COLUMN p_price_discount DECIMAL(12, 2);


