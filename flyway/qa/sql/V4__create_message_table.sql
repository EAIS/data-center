CREATE TABLE IF NOT EXISTS message (
  m_id          BIGINT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  u_receiver_id BIGINT             NOT NULL,
  u_sender_id   BIGINT             NOT NULL,
  p_id          BIGINT             NOT NULL,
  m_hasread     BOOLEAN            NOT NULL,
  FOREIGN KEY (u_receiver_id) REFERENCES user_info (u_id)
    ON DELETE CASCADE,
  FOREIGN KEY (u_sender_id) REFERENCES user_info (u_id)
    ON DELETE CASCADE,
  FOREIGN KEY (p_id) REFERENCES product (p_id)
    ON DELETE CASCADE
);


