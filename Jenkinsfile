def gitUrl = 'git@gitlab.com:EAIS/data-center.git'

def envMap = [qa: 'qa']
def portMap = [qa: 9091]

pipeline {
    agent any
    stages {
        stage('build & publish') {
            steps {
                checkoutGitRepo(gitUrl)
                sh 'gradle flywayMigrate -Dprofile=test'
                sh 'gradle clean build'
            }
        }

        stage('deploy to QA') {
            steps {
                removeContainerIfNeeded("data-center-${envMap['qa']}")
                sh "gradle flywayMigrate -Dprofile=${envMap['qa']}"
                sh "docker build -f docker/${envMap['qa']}/Dockerfile -t data-center-${envMap['qa']} ."
                sh "docker run \
                    --privileged \
                    -p ${portMap['qa']}:${portMap['qa']} \
                    -v /tmp/eais_qa_images:/tmp/eais_images \
                    -v /tmp/eais_qa_logs:/tmp/logs \
                    -v /etc/localtime:/etc/localtime \
                    --name data-center-${envMap['qa']} \
                    data-center-${envMap['qa']} \
                    --net host &"
            }
        }

    }
}

def removeContainerIfNeeded(containerName) {
    sh "chmod +x removeContainerIfNeeded.sh"
    sh "./removeContainerIfNeeded.sh ${containerName}"
}

def checkoutGitRepo(gitUrl) {
    checkout([$class                           : 'GitSCM',
              branches                         : [[name: '*/master']],
              userRemoteConfigs                : [[url: gitUrl]]
    ])
}