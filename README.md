#DATA-CENTER

###How to Build
1. using flyway migration the database:
- local: ```gradle flywayMigrate -Dprofile=local```
- qa: ```gradle flywayMigrate -Dprofile=qa```
- test: ```gradle flywayMigrate -Dprofile=test```

2. using gradle build jar file:
- ```gradle clean build```

###How to Start Server
1. using java commander start server:
- local: ```java -jar -Dspring.profiles.active=local eais-data-center-0.0.1.jar --server.port=9090```
- qa: ```java -jar -Dspring.profiles.active=qa eais-data-center-0.0.1.jar --server.port=9091```

###How to Debug Server
1. using commander ```gradle bootRun --debug-jvm```
2. create remote in the running configuration
3. when gradle bootRun --debug-jvm commander suspended, choose remote mode and click debug button


###Handle Docker server
1. using command ``` docker exec -it {container_id} sh``` to login into server machine
2. using command ```sudo ifconfig lo0 alias 123.123.123.123/24``` add alias to localhost let docker machine visit local database 