package cn.com.eais.validator.product;

import cn.com.eais.entities.Product;
import cn.com.eais.entities.UserInfo;
import cn.com.eais.model.ErrorMessage;
import cn.com.eais.validator.ValidatorContext;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static cn.com.eais.commons.ProductBuilder.CREATE_DEFAULT_PRODUCT;
import static cn.com.eais.contants.ErrorsProperties.EMPTY_ERROR;
import static cn.com.eais.contants.ProductProperties.*;
import static org.assertj.core.api.Assertions.assertThat;

public class ProductValidatorFacadeTest {
    private ValidatorContext<Product> productValidator;
    private Product product;

    @Before
    public void setUp() throws Exception {
        product = CREATE_DEFAULT_PRODUCT(new UserInfo());
        productValidator = new ProductValidatorFacade().newProductValidator();
    }

    @Test
    public void shouldReturnEmptyErrorMessageWhenValidateNoErrors() {
        product.getUserInfo().setId(1L);

        //When
        final List<ErrorMessage> result = productValidator.validate(product);

        //Then
        assertThat(result.size()).isEqualTo(0);
    }

    @Test
    public void shouldReturnErrorWhenFieldIsIllegal() {
        //Given
        product.setTitle(null);
        product.setLocation(null);
        product.setPriceOrigin(null);
        product.setPriceDiscount(null);
        product.setStock(null);
        product.setValidDate(null);

        //When
        final List<ErrorMessage> result = productValidator.validate(product);

        //Then
        assertThat(result.size()).isEqualTo(7);
        assertThat(result.get(0))
                .hasFieldOrPropertyWithValue("field", USER_ID_FIELD)
                .hasFieldOrPropertyWithValue("message", EMPTY_ERROR);
        assertThat(result.get(1))
                .hasFieldOrPropertyWithValue("field", TITLE_FIELD)
                .hasFieldOrPropertyWithValue("message", EMPTY_ERROR);
        assertThat((result.get(2)))
                .hasFieldOrPropertyWithValue("field", LOCATION_FIELD)
                .hasFieldOrPropertyWithValue("message", EMPTY_ERROR);
        assertThat(result.get(3))
                .hasFieldOrPropertyWithValue("field", ORIGIN_PRICE_FIELD)
                .hasFieldOrPropertyWithValue("message", EMPTY_ERROR);
        assertThat(result.get(4))
                .hasFieldOrPropertyWithValue("field", DISCOUNT_PRICE_FIELD)
                .hasFieldOrPropertyWithValue("message", EMPTY_ERROR);
        assertThat(result.get(5))
                .hasFieldOrPropertyWithValue("field", VALID_DATE_FIELD)
                .hasFieldOrPropertyWithValue("message", EMPTY_ERROR);
        assertThat(result.get(6))
                .hasFieldOrPropertyWithValue("field", STOCK_FIELD)
                .hasFieldOrPropertyWithValue("message", EMPTY_ERROR);

    }
}