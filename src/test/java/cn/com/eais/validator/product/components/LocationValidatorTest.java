package cn.com.eais.validator.product.components;

import cn.com.eais.entities.Product;
import cn.com.eais.entities.UserInfo;
import cn.com.eais.model.ErrorMessage;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static cn.com.eais.commons.ProductBuilder.CREATE_DEFAULT_PRODUCT;
import static cn.com.eais.contants.ErrorsProperties.EMPTY_ERROR;
import static cn.com.eais.contants.ErrorsProperties.ILLEGAL_ERROR;
import static cn.com.eais.contants.ProductProperties.LOCATION_FIELD;
import static org.assertj.core.api.Assertions.assertThat;

public class LocationValidatorTest {

    private LocationValidator validator;
    private Product product;

    @Before
    public void setUp() throws Exception {
        validator = new LocationValidator();
        product = CREATE_DEFAULT_PRODUCT(new UserInfo());
    }

    @Test
    public void shouldReturnNoErrorsWhenAllTheDataIsValid() {
        //When
        List<ErrorMessage> results = validator.validate(product);

        //Then
        assertThat(results.size()).isEqualTo(0);
    }

    @Test
    public void shouldReturnEmptyErrorWhenLocationIsEmpty() {
        product.setLocation(null);

        //When
        List<ErrorMessage> results = validator.validate(product);

        //Then
        assertThat(results.size()).isEqualTo(1);
        assertThat(results.get(0))
                .hasFieldOrPropertyWithValue("field", LOCATION_FIELD)
                .hasFieldOrPropertyWithValue("message", EMPTY_ERROR);

    }

    @Test
    public void shouldReturnIllegalWhenLocationLengthIsGreaterThan100() {
        //Given
        product.setLocation(RandomStringUtils.random(101));

        //When
        List<ErrorMessage> results = validator.validate(product);

        //Then
        assertThat(results.size()).isEqualTo(1);
        assertThat(results.get(0))
                .hasFieldOrPropertyWithValue("field", LOCATION_FIELD)
                .hasFieldOrPropertyWithValue("message", ILLEGAL_ERROR);
    }
}