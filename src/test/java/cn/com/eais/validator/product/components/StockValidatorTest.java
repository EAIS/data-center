package cn.com.eais.validator.product.components;

import cn.com.eais.entities.Product;
import cn.com.eais.entities.UserInfo;
import cn.com.eais.model.ErrorMessage;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static cn.com.eais.commons.ProductBuilder.CREATE_DEFAULT_PRODUCT;
import static cn.com.eais.contants.ErrorsProperties.EMPTY_ERROR;
import static cn.com.eais.contants.ProductProperties.STOCK_FIELD;
import static org.assertj.core.api.Assertions.assertThat;

public class StockValidatorTest {
    private StockValidator validator;
    private Product product;

    @Before
    public void setUp() throws Exception {
        validator = new StockValidator();
        product = CREATE_DEFAULT_PRODUCT(new UserInfo());
    }

    @Test
    public void shouldReturnNoErrorsWhenAllTheDataIsValid() {
        //Given
        product.getUserInfo().setId(1L);

        //When
        List<ErrorMessage> results = validator.validate(product);

        //Then
        assertThat(results.size()).isEqualTo(0);
    }

    @Test
    public void shouldReturnErrorsWhenStockIsEmpty() {
        product.setStock(null);

        //When
        List<ErrorMessage> results = validator.validate(product);

        //Then
        assertThat(results.size()).isEqualTo(1);
        assertThat(results.get(0))
                .hasFieldOrPropertyWithValue("field", STOCK_FIELD)
                .hasFieldOrPropertyWithValue("message", EMPTY_ERROR);

    }
}