package cn.com.eais.validator.product.components;

import cn.com.eais.entities.Product;
import cn.com.eais.entities.UserInfo;
import cn.com.eais.model.ErrorMessage;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static cn.com.eais.commons.ProductBuilder.CREATE_DEFAULT_PRODUCT;
import static cn.com.eais.contants.ErrorsProperties.EMPTY_ERROR;
import static cn.com.eais.contants.ProductProperties.DISCOUNT_PRICE_FIELD;
import static cn.com.eais.contants.ProductProperties.ORIGIN_PRICE_FIELD;
import static org.assertj.core.api.Assertions.assertThat;

public class PriceValidatorTest {
    private PriceValidator validator;
    private Product product;

    @Before
    public void setUp() throws Exception {
        validator = new PriceValidator();
        product = CREATE_DEFAULT_PRODUCT(new UserInfo());
    }

    @Test
    public void shouldReturnNoErrorsWhenAllTheDataIsValid() {
        //Given
        product.getUserInfo().setId(1L);

        //When
        List<ErrorMessage> results = validator.validate(product);

        //Then
        assertThat(results.size()).isEqualTo(0);
    }

    @Test
    public void shouldReturnEmptyErrorsWhenOriginPriceAndDiscountPriceAreEmpty() {
        product.setPriceOrigin(null);
        product.setPriceDiscount(null);

        //When
        List<ErrorMessage> results = validator.validate(product);

        //Then
        assertThat(results.size()).isEqualTo(2);
        assertThat(results.get(0))
                .hasFieldOrPropertyWithValue("field", ORIGIN_PRICE_FIELD)
                .hasFieldOrPropertyWithValue("message", EMPTY_ERROR);
        assertThat(results.get(1))
                .hasFieldOrPropertyWithValue("field", DISCOUNT_PRICE_FIELD)
                .hasFieldOrPropertyWithValue("message", EMPTY_ERROR);
    }
}