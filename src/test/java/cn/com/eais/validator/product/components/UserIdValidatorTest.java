package cn.com.eais.validator.product.components;

import cn.com.eais.entities.Product;
import cn.com.eais.entities.UserInfo;
import cn.com.eais.model.ErrorMessage;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static cn.com.eais.commons.ProductBuilder.CREATE_DEFAULT_PRODUCT;
import static cn.com.eais.contants.ErrorsProperties.EMPTY_ERROR;
import static cn.com.eais.contants.ProductProperties.USER_ID_FIELD;
import static org.assertj.core.api.Assertions.assertThat;

public class UserIdValidatorTest {

    private UserIdValidator validator;
    private Product product;

    @Before
    public void setUp() throws Exception {
        validator = new UserIdValidator();
        product = CREATE_DEFAULT_PRODUCT(new UserInfo());
    }

    @Test
    public void shouldReturnNoErrorsWhenAllTheDataIsValid() {
        //Given
        product.getUserInfo().setId(1L);

        //When
        List<ErrorMessage> results = validator.validate(product);

        //Then
        assertThat(results.size()).isEqualTo(0);
    }

    @Test
    public void shouldReturnErrorsWhenUserIdIsEmpty() {
        //When
        List<ErrorMessage> results = validator.validate(product);

        //Then
        assertThat(results.size()).isEqualTo(1);
        assertThat(results.get(0))
                .hasFieldOrPropertyWithValue("field", USER_ID_FIELD)
                .hasFieldOrPropertyWithValue("message", EMPTY_ERROR);

    }
}