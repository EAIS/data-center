package cn.com.eais.validator.product.components;

import cn.com.eais.entities.Product;
import cn.com.eais.entities.UserInfo;
import cn.com.eais.model.ErrorMessage;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static cn.com.eais.commons.ProductBuilder.CREATE_DEFAULT_PRODUCT;
import static cn.com.eais.contants.ErrorsProperties.EMPTY_ERROR;
import static cn.com.eais.contants.ErrorsProperties.ILLEGAL_ERROR;
import static cn.com.eais.contants.ProductProperties.TITLE_FIELD;
import static org.assertj.core.api.Assertions.assertThat;

public class TitleValidatorTest {

    private TitleValidator validator;
    private Product product;

    @Before
    public void setUp() throws Exception {
        validator = new TitleValidator();
        product = CREATE_DEFAULT_PRODUCT(new UserInfo());
    }

    @Test
    public void shouldReturnNoErrorsWhenAllTheDataIsValid() {
        //When
        List<ErrorMessage> results = validator.validate(product);

        //Then
        assertThat(results.size()).isEqualTo(0);
    }

    @Test
    public void shouldReturnEmptyErrorWhenTitleIsEmpty() {
        product.setTitle(null);

        //When
        List<ErrorMessage> results = validator.validate(product);

        //Then
        assertThat(results.size()).isEqualTo(1);
        assertThat(results.get(0))
                .hasFieldOrPropertyWithValue("field", TITLE_FIELD)
                .hasFieldOrPropertyWithValue("message", EMPTY_ERROR);

    }

    @Test
    public void shouldReturnIllegalWhenTitleLengthIsGreaterThan100() {
        //Given
        product.setTitle(RandomStringUtils.random(101));

        //When
        List<ErrorMessage> results = validator.validate(product);

        //Then
        assertThat(results.size()).isEqualTo(1);
        assertThat(results.get(0))
                .hasFieldOrPropertyWithValue("field", TITLE_FIELD)
                .hasFieldOrPropertyWithValue("message", ILLEGAL_ERROR);
    }
}