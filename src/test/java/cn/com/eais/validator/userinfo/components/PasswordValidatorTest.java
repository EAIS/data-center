package cn.com.eais.validator.userinfo.components;

import cn.com.eais.entities.UserInfo;
import cn.com.eais.model.ErrorMessage;
import cn.com.eais.validator.ValidatorContext;
import cn.com.eais.validator.userinfo.UserInfoValidatorFacade;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static cn.com.eais.commons.UserInfoBuilder.defaultValidUserInfo;
import static cn.com.eais.contants.ErrorsProperties.EMPTY_ERROR;
import static cn.com.eais.contants.ErrorsProperties.ILLEGAL_ERROR;
import static cn.com.eais.contants.UserInfoProperties.*;
import static org.assertj.core.api.Assertions.assertThat;

public class PasswordValidatorTest {
    private PasswordValidator userInfoValidator;
    private UserInfo userInfo;

    @Before
    public void setUp() throws Exception {
        userInfo = defaultValidUserInfo();
        userInfoValidator = new PasswordValidator();
    }

    @Test
    public void shouldReturnEmptyErrorMessageWhenValidateNoErrors() {
        //When
        final List<ErrorMessage> result = userInfoValidator.validate(userInfo);

        //Then
        assertThat(result.size()).isEqualTo(0);
    }

    @Test
    public void shouldReturnPasswordEmptyErrorWhenPasswordIsEmpty() {
        //Given
        userInfo.setPassword(null);

        //When
        final List<ErrorMessage> result = userInfoValidator.validate(userInfo);

        //Then
        assertThat(result.size()).isEqualTo(1);
        assertThat(result.get(0))
                .hasFieldOrPropertyWithValue("field", PASSWORD_FIELD)
                .hasFieldOrPropertyWithValue("message", EMPTY_ERROR);
    }

    @Test
    public void shouldReturnPasswordIllegalErrorWhenPasswordLengthIsGreaterThan64() {
        //Given
        userInfo.setPassword(RandomStringUtils.random(65));

        //When
        final List<ErrorMessage> result = userInfoValidator.validate(userInfo);

        //Then
        assertThat(result.size()).isEqualTo(1);
        assertThat((result.get(0)))
                .hasFieldOrPropertyWithValue("field", PASSWORD_FIELD)
                .hasFieldOrPropertyWithValue("message", ILLEGAL_ERROR);
    }
}