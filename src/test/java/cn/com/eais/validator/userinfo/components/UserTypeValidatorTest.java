package cn.com.eais.validator.userinfo.components;

import cn.com.eais.entities.UserInfo;
import cn.com.eais.model.ErrorMessage;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static cn.com.eais.commons.UserInfoBuilder.defaultValidUserInfo;
import static cn.com.eais.contants.ErrorsProperties.EMPTY_ERROR;
import static cn.com.eais.contants.UserInfoProperties.*;
import static org.assertj.core.api.Assertions.assertThat;

public class UserTypeValidatorTest {
    private UserTypeValidator userInfoValidator;
    private UserInfo userInfo;

    @Before
    public void setUp() throws Exception {
        userInfo = defaultValidUserInfo();
        userInfoValidator = new UserTypeValidator();
    }

    @Test
    public void shouldReturnEmptyErrorMessageWhenValidateNoErrors() {
        //When
        final List<ErrorMessage> result = userInfoValidator.validate(userInfo);

        //Then
        assertThat(result.size()).isEqualTo(0);
    }

    @Test
    public void shouldReturnTypeEmptyErrorWhenTypeIsEmpty() {
        //Given
        userInfo.setType(null);

        //When
        final List<ErrorMessage> result = userInfoValidator.validate(userInfo);

        //Then
        assertThat(result.size()).isEqualTo(1);
        assertThat(result.get(0))
                .hasFieldOrPropertyWithValue("field", TYPE_FIELD)
                .hasFieldOrPropertyWithValue("message", EMPTY_ERROR);
    }
}