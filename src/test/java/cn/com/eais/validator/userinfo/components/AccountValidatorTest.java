package cn.com.eais.validator.userinfo.components;

import cn.com.eais.entities.UserInfo;
import cn.com.eais.model.ErrorMessage;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static cn.com.eais.commons.UserInfoBuilder.defaultValidUserInfo;
import static cn.com.eais.contants.ErrorsProperties.ILLEGAL_ERROR;
import static cn.com.eais.contants.UserInfoProperties.ACCOUNT_FIELD;
import static org.assertj.core.api.Assertions.assertThat;

public class AccountValidatorTest {
    private AccountValidator userInfoValidator;
    private UserInfo userInfo;

    @Before
    public void setUp() throws Exception {
        userInfo = defaultValidUserInfo();
        userInfoValidator = new AccountValidator();
    }

    @Test
    public void shouldReturnEmptyErrorMessageWhenValidateNoErrors() {
        //When
        final List<ErrorMessage> result = userInfoValidator.validate(userInfo);

        //Then
        assertThat(result.size()).isEqualTo(0);
    }

    @Test
    public void shouldReturnAccountIllegalErrorWhenAccountLengthIsGreaterThan50() {
        //Given
        userInfo.setAccount(RandomStringUtils.random(51));

        //When
        final List<ErrorMessage> result = userInfoValidator.validate(userInfo);

        //Then
        assertThat(result.size()).isEqualTo(1);
        assertThat(result.get(0))
                .hasFieldOrPropertyWithValue("field", ACCOUNT_FIELD)
                .hasFieldOrPropertyWithValue("message", ILLEGAL_ERROR);
    }
}