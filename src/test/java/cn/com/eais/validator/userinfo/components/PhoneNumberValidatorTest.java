package cn.com.eais.validator.userinfo.components;

import cn.com.eais.entities.UserInfo;
import cn.com.eais.model.ErrorMessage;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static cn.com.eais.commons.UserInfoBuilder.defaultValidUserInfo;
import static cn.com.eais.contants.ErrorsProperties.EMPTY_ERROR;
import static cn.com.eais.contants.ErrorsProperties.ILLEGAL_ERROR;
import static cn.com.eais.contants.UserInfoProperties.*;
import static org.assertj.core.api.Assertions.assertThat;

public class PhoneNumberValidatorTest {
    private PhoneNumberValidator userInfoValidator;
    private UserInfo userInfo;

    @Before
    public void setUp() throws Exception {
        userInfo = defaultValidUserInfo();
        userInfoValidator = new PhoneNumberValidator();
    }

    @Test
    public void shouldReturnEmptyErrorMessageWhenValidateNoErrors() {
        //When
        final List<ErrorMessage> result = userInfoValidator.validate(userInfo);

        //Then
        assertThat(result.size()).isEqualTo(0);
    }

    @Test
    public void shouldReturnPhoneNumberNullErrorWhenPhoneNumberIsNull() {
        userInfo.setPhoneNumber(null);

        //When
        final List<ErrorMessage> result = userInfoValidator.validate(userInfo);

        //Then
        assertThat(result.size()).isEqualTo(1);
        assertThat(result.get(0))
                .hasFieldOrPropertyWithValue("field", PHONE_NUMBER_FIELD)
                .hasFieldOrPropertyWithValue("message", EMPTY_ERROR);
    }

    @Test
    public void shouldReturnPhoneNumberIllegalErrorWhenPhoneNumberIsIllegal() {
        //Given
        userInfo.setPhoneNumber("189xxxx1212");

        //When
        final List<ErrorMessage> result = userInfoValidator.validate(userInfo);

        //Then
        assertThat(result.size()).isEqualTo(1);
        assertThat(result.get(0))
                .hasFieldOrPropertyWithValue("field", PHONE_NUMBER_FIELD)
                .hasFieldOrPropertyWithValue("message", ILLEGAL_ERROR);
    }
}