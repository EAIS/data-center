package cn.com.eais.validator.userinfo;

import cn.com.eais.entities.UserInfo;
import cn.com.eais.model.ErrorMessage;
import cn.com.eais.validator.ValidatorContext;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static cn.com.eais.commons.UserInfoBuilder.defaultValidUserInfo;
import static cn.com.eais.contants.ErrorsProperties.EMPTY_ERROR;
import static cn.com.eais.contants.ErrorsProperties.ILLEGAL_ERROR;
import static cn.com.eais.contants.UserInfoProperties.*;
import static org.assertj.core.api.Assertions.assertThat;

public class UserInfoValidatorFacadeTest {

    private ValidatorContext<UserInfo> userInfoValidator;
    private UserInfo userInfo;

    @Before
    public void setUp() throws Exception {
        userInfo = defaultValidUserInfo();
        userInfoValidator = new UserInfoValidatorFacade().registryUserInfoValidator();
    }

    @Test
    public void shouldReturnEmptyErrorMessageWhenValidateNoErrors() {
        //When
        final List<ErrorMessage> result = userInfoValidator.validate(userInfo);

        //Then
        assertThat(result.size()).isEqualTo(0);
    }

    @Test
    public void shouldReturnErrorWhenFieldIsIllegal() {
        //Given
        userInfo.setPhoneNumber("189xxxx1212");
        userInfo.setAccount(RandomStringUtils.random(51));
        userInfo.setPassword(RandomStringUtils.random(65));
        userInfo.setName(RandomStringUtils.random(21));
        userInfo.setIdentity("4309221991010100");
        userInfo.setAddress(RandomStringUtils.random(101));
        userInfo.setType(null);

        //When
        final List<ErrorMessage> result = userInfoValidator.validate(userInfo);

        //Then
        assertThat(result.size()).isEqualTo(7);
        assertThat(result.get(0))
                .hasFieldOrPropertyWithValue("field", PHONE_NUMBER_FIELD)
                .hasFieldOrPropertyWithValue("message", ILLEGAL_ERROR);
        assertThat(result.get(1))
                .hasFieldOrPropertyWithValue("field", ACCOUNT_FIELD)
                .hasFieldOrPropertyWithValue("message", ILLEGAL_ERROR);
        assertThat((result.get(2)))
                .hasFieldOrPropertyWithValue("field", PASSWORD_FIELD)
                .hasFieldOrPropertyWithValue("message", ILLEGAL_ERROR);
        assertThat(result.get(3))
                .hasFieldOrPropertyWithValue("field", NAME_FIELD)
                .hasFieldOrPropertyWithValue("message", ILLEGAL_ERROR);
        assertThat(result.get(4))
                .hasFieldOrPropertyWithValue("field", IDENTITY_FIELD)
                .hasFieldOrPropertyWithValue("message", ILLEGAL_ERROR);
        assertThat(result.get(5))
                .hasFieldOrPropertyWithValue("field", ADDRESS_FIELD)
                .hasFieldOrPropertyWithValue("message", ILLEGAL_ERROR);
        assertThat(result.get(6))
                .hasFieldOrPropertyWithValue("field", TYPE_FIELD)
                .hasFieldOrPropertyWithValue("message", EMPTY_ERROR);

    }

}