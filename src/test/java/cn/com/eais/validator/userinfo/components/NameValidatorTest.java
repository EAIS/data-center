package cn.com.eais.validator.userinfo.components;

import cn.com.eais.entities.UserInfo;
import cn.com.eais.model.ErrorMessage;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static cn.com.eais.commons.UserInfoBuilder.defaultValidUserInfo;
import static cn.com.eais.contants.ErrorsProperties.ILLEGAL_ERROR;
import static cn.com.eais.contants.UserInfoProperties.*;
import static org.assertj.core.api.Assertions.assertThat;

public class NameValidatorTest {

    private NameValidator userInfoValidator;
    private UserInfo userInfo;

    @Before
    public void setUp() throws Exception {
        userInfo = defaultValidUserInfo();
        userInfoValidator = new NameValidator();
    }

    @Test
    public void shouldReturnEmptyErrorMessageWhenValidateNoErrors() {
        //When
        final List<ErrorMessage> result = userInfoValidator.validate(userInfo);

        //Then
        assertThat(result.size()).isEqualTo(0);
    }

    @Test
    public void shouldReturnNameIllegalErrorWhenNameLengthIsGreaterThan20() {
        //Given
        userInfo.setName(RandomStringUtils.random(21));

        //When
        final List<ErrorMessage> result = userInfoValidator.validate(userInfo);

        //Then
        assertThat(result.size()).isEqualTo(1);
        assertThat(result.get(0))
                .hasFieldOrPropertyWithValue("field", NAME_FIELD)
                .hasFieldOrPropertyWithValue("message", ILLEGAL_ERROR);

    }
}