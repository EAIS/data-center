package cn.com.eais.validator.request.header;

import cn.com.eais.model.ErrorMessage;
import cn.com.eais.model.RequestHeaders;
import cn.com.eais.validator.ValidatorContext;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static cn.com.eais.commons.HttpHeadersBuilder.defaultRequestHeaders;
import static cn.com.eais.contants.CorsProperties.*;
import static cn.com.eais.contants.CorsProperties.X_CLIENT_KEY;
import static cn.com.eais.contants.CorsProperties.X_CLIENT_KEY_MATCH_ERROR;
import static org.assertj.core.api.Assertions.assertThat;

public class RequestHeaderValidatorFacadeTest {

    private final String CLIENT_NAME = "middleLayer";
    private final String CLIENT_KEY = "hello";
    private ValidatorContext<RequestHeaders> validator;

    @Before
    public void setUp() throws Exception {
        validator = new RequestHeaderValidatorFacade(CLIENT_KEY).requestHeaderValidator();
    }

    @Test
    public void shouldReturnErrorMessageListWithSize0WhenRequestHeaderWithNoError() {
        //Given
        RequestHeaders headers = defaultRequestHeaders("1", CLIENT_NAME, "1", CLIENT_KEY);

        //When
        final List<ErrorMessage> messages = validator.validate(headers);

        //Then
        assertThat(messages.size()).isEqualTo(0);
    }

    @Test
    public void shouldReturnCorrelationIdErrorMessage() {
        //Given
        RequestHeaders headers = defaultRequestHeaders(null, "123", null, "world");

        //When
        final List<ErrorMessage> messages = validator.validate(headers);

        //Then
        assertThat(messages.size()).isEqualTo(4);
        assertThat(messages.get(0))
                .hasFieldOrPropertyWithValue("field", X_CORRELATION_ID)
                .hasFieldOrPropertyWithValue("message", X_CORRELATION_ID_EMPTY_ERROR);
        assertThat(messages.get(1))
                .hasFieldOrPropertyWithValue("field", X_CLIENT_NAME)
                .hasFieldOrPropertyWithValue("message", X_CLIENT_NAME_MATCH_ERROR);
        assertThat(messages.get(2))
                .hasFieldOrPropertyWithValue("field", X_CLIENT_VERSION)
                .hasFieldOrPropertyWithValue("message", X_CLIENT_VERSION_EMPTY_ERROR);
        assertThat(messages.get(3))
                .hasFieldOrPropertyWithValue("field", X_CLIENT_KEY)
                .hasFieldOrPropertyWithValue("message", X_CLIENT_KEY_MATCH_ERROR);
    }
}