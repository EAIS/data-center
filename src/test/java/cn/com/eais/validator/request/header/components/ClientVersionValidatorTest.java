package cn.com.eais.validator.request.header.components;

import cn.com.eais.model.ErrorMessage;
import cn.com.eais.model.RequestHeaders;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static cn.com.eais.commons.HttpHeadersBuilder.defaultRequestHeaders;
import static cn.com.eais.contants.CorsProperties.*;
import static org.assertj.core.api.Assertions.assertThat;

public class ClientVersionValidatorTest {
    private final String CLIENT_NAME = "middleLayer";
    private final String CLIENT_KEY = "hello";
    private ClientVersionValidator validator;

    @Before
    public void setUp() throws Exception {
        validator = new ClientVersionValidator();
    }

    @Test
    public void shouldReturnErrorMessageListWithSize0WhenRequestHeaderWithNoError() {
        //Given
        RequestHeaders headers = defaultRequestHeaders("1", CLIENT_NAME, "1", CLIENT_KEY);

        //When
        final List<ErrorMessage> messages = validator.validate(headers);

        //Then
        assertThat(messages.size()).isEqualTo(0);
    }

    @Test
    public void shouldReturnClientVersionErrorMessage() {
        //Given
        RequestHeaders headers = defaultRequestHeaders("1", CLIENT_NAME, null, CLIENT_KEY);

        //When
        final List<ErrorMessage> messages = validator.validate(headers);

        //Then
        assertThat(messages.size()).isEqualTo(1);
        assertThat(messages.get(0))
                .hasFieldOrPropertyWithValue("field", X_CLIENT_VERSION)
                .hasFieldOrPropertyWithValue("message", X_CLIENT_VERSION_EMPTY_ERROR);
    }
}