package cn.com.eais.repositories;

import cn.com.eais.entities.Product;
import cn.com.eais.entities.ShoppingCart;
import cn.com.eais.entities.UserInfo;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.List;

import static cn.com.eais.commons.ProductBuilder.CREATE_DEFAULT_PRODUCT;
import static cn.com.eais.commons.UserInfoBuilder.userInfoForJpaTesting;
import static cn.com.eais.enums.UserType.NORMAL;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = NONE)
public class ShoppingCartRepositoryTest {

    @Autowired
    private UserInfoRepository userInfoRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private ShoppingCartRepository shoppingCartRepository;


    private UserInfo userInfo;
    private Product product;
    private ShoppingCart shoppingCart;

    @Before
    public void setUp() throws Exception {
        userInfo = userInfoRepository.save(userInfoForJpaTesting());
        product = productRepository.save(CREATE_DEFAULT_PRODUCT(userInfo));
        shoppingCart = new ShoppingCart();
        shoppingCart.setUserInfo(userInfo);
        shoppingCart.setProduct(product);
        shoppingCart.setCount(5);
    }

    @Test
    public void shouldReturnShoppingCartAfterSaveIt() {
        //When
        ShoppingCart savedShoppingCart = shoppingCartRepository.save(shoppingCart);

        //Then
        assertThat(savedShoppingCart.getId()).isNotNull();
        defaultAssertForDefaultUserInfoTesting(savedShoppingCart.getUserInfo());
        defaultAssertForDefaultProductTesting(savedShoppingCart.getProduct());
        assertThat(savedShoppingCart).hasFieldOrPropertyWithValue("count", 5);
    }

    @Test
    public void shouldReturnNewShoppingCartAfterUpdateIt() {
        //Given
        ShoppingCart savedShoppingCart = shoppingCartRepository.save(shoppingCart);

        //When
        shoppingCartRepository.updateCountById(savedShoppingCart.getId(), 10);
        savedShoppingCart = shoppingCartRepository.findById(savedShoppingCart.getId());

        //Then
        assertThat(savedShoppingCart).hasFieldOrPropertyWithValue("count", 10);
    }

    @Test
    public void shouldReturnShoppingCartWhenFindByUserInfo() {
        //Given
        shoppingCartRepository.save(shoppingCart);

        //When
        List<ShoppingCart> shoppingCartList = shoppingCartRepository.findByUserInfo(userInfo);

        //Then
        assertThat(shoppingCartList.get(0).getId()).isNotNull();
        defaultAssertForDefaultUserInfoTesting(shoppingCartList.get(0).getUserInfo());
        defaultAssertForDefaultProductTesting(shoppingCartList.get(0).getProduct());
        assertThat(shoppingCartList.get(0)).hasFieldOrPropertyWithValue("count", 5);
    }

    private void defaultAssertForDefaultProductTesting(Product saveProduct) {
        assertThat(saveProduct.getApprove()).isNotNull();
        assertThat(saveProduct.getUserInfo().getId()).isEqualTo(userInfo.getId());
        assertThat(saveProduct)
                .hasFieldOrPropertyWithValue("title", "test_title")
                .hasFieldOrPropertyWithValue("image", null)
                .hasFieldOrPropertyWithValue("location", "test_location")
                .hasFieldOrPropertyWithValue("priceOrigin", BigDecimal.valueOf(20.8))
                .hasFieldOrPropertyWithValue("priceDiscount", BigDecimal.valueOf(10.0))
                .hasFieldOrPropertyWithValue("validDate", DateTime.parse("2017-05-31").toDate())
                .hasFieldOrPropertyWithValue("valid", true)
                .hasFieldOrPropertyWithValue("approve", true)
                .hasFieldOrPropertyWithValue("stock", 5)
                .hasFieldOrPropertyWithValue("deleted", false);
    }

    private void defaultAssertForDefaultUserInfoTesting(UserInfo userInfo) {
        assertThat(userInfo.getId()).isNotNull();
        assertThat(userInfo)
                .hasFieldOrPropertyWithValue("phoneNumber", "189xxxx8888")
                .hasFieldOrPropertyWithValue("account", "mentu")
                .hasFieldOrPropertyWithValue("password", "gsy")
                .hasFieldOrPropertyWithValue("type", NORMAL)
                .hasFieldOrPropertyWithValue("name", null)
                .hasFieldOrPropertyWithValue("identity", null)
                .hasFieldOrPropertyWithValue("address", null)
                .hasFieldOrPropertyWithValue("deleted", false);
    }

}
