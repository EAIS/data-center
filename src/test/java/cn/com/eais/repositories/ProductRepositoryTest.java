package cn.com.eais.repositories;

import cn.com.eais.entities.Product;
import cn.com.eais.entities.UserInfo;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.List;

import static cn.com.eais.commons.ProductBuilder.CREATE_DEFAULT_PRODUCT;
import static cn.com.eais.commons.UserInfoBuilder.userInfoForJpaTesting;
import static cn.com.eais.enums.UserType.NORMAL;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = NONE)
public class ProductRepositoryTest {

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private UserInfoRepository userInfoRepository;

    private UserInfo userInfo;

    private Product product;

    @Before
    public void setUp() throws Exception {
        userInfo = userInfoRepository.save(userInfoForJpaTesting());
        product = CREATE_DEFAULT_PRODUCT(userInfo);
    }

    @Test
    public void shouldReturnProductAfterSaveIt() {
        //When
        Product saveProduct = productRepository.save(product);

        //Then
        defaultAssertForDefaultProductTesting(saveProduct);
    }

    @Test
    public void shouldReturnProductWhenSelectById() {
        //Given
        Product savedProduct = productRepository.save(product);

        //When
        Product testProduct = productRepository.findByIdAndDeleted(savedProduct.getId(), false);

        //Then
        defaultAssertForDefaultProductTesting(testProduct);
        defaultAssertForDefaultUserInfoTesting(testProduct.getUserInfo());
    }

    @Test
    public void shouldReturnProductWhenSelectByUserInfo() {
        //Given
        productRepository.save(product);

        //When
        List<Product> testProduct = productRepository.findByUserInfoAndValidAndApproveAndDeleted(userInfo, true, true, false);

        //Then
        defaultAssertForDefaultProductTesting(testProduct.get(0));
    }

    @Test
    public void shouldReturnProductWhenSelectByPTitle() {
        //Given
        productRepository.save(product);

        //When
        List<Product> testProduct = productRepository.findByTitleContainingAndValidAndApproveAndDeleted("tit", true, true, false);

        //Then
        defaultAssertForDefaultProductTesting(testProduct.get(0));
    }

    @Test
    public void shouldReturnProductWhenSelectByPLocation() {
        //Given
        productRepository.save(product);

        //When
        List<Product> testProduct = productRepository.findByLocationContainingAndValidAndApproveAndDeleted("location", true, true, false);

        //Then
        defaultAssertForDefaultProductTesting(testProduct.get(0));
    }

    @Test
    public void shouldReturnProductWhenSelectByPPriceDiscount() {
        //Given
        productRepository.save(product);

        //When
        List<Product> testProduct = productRepository.findByPriceDiscountBetweenAndValidAndApproveAndDeleted(BigDecimal.valueOf(5.0), BigDecimal.valueOf(15.0), true, true, false);

        //Then
        defaultAssertForDefaultProductTesting(testProduct.get(0));
    }

    @Test
    public void shouldReturnProductWhenSelectByPValidDate() {
        //Given
        productRepository.save(product);

        //When
        List<Product> testValidProduct = productRepository.findByValidDateLessThanEqualAndValidAndApproveAndDeleted(DateTime.parse("2017-05-31").toDate(), true, true, false);
        List<Product> testInvalidProduct = productRepository.findByValidDateLessThanEqualAndValidAndApproveAndDeleted(DateTime.parse("2017-05-30").toDate(), true, true, false);

        //Then
        defaultAssertForDefaultProductTesting(testValidProduct.get(0));
        assertThat(testInvalidProduct.size()).isEqualTo(0);
    }

    @Test
    public void shouldReturnProductWhenSelectByPValid() {
        //Given
        productRepository.save(product);

        //When
        List<Product> testProduct = productRepository.findByValidAndApproveAndDeleted(true, true, false);

        //Then
        defaultAssertForDefaultProductTesting(testProduct.get(0));
    }

    @Test
    public void shouldReturnProductWhenSelectByPApprove() {
        //Given
        productRepository.save(product);

        //When
        List<Product> testProduct = productRepository.findByApproveAndDeleted(true, false);

        //Then
        defaultAssertForDefaultProductTesting(testProduct.get(0));
    }

    @Test
    public void shouldReturnProductWhenSelectByDeleted() {
        //Given
        productRepository.save(product);

        //When
        List<Product> testProduct = productRepository.findByDeleted(false);

        //Then
        defaultAssertForDefaultProductTesting(testProduct.get(0));
    }

    @Test
    public void shouldReturnNewProductWithUpdateApproveFlagSetToTrue() {
        //Given
        product.setApprove(false);
        final Product savedProduct = productRepository.save(product);

        //When
        productRepository.updateApproveFlag();

        //Then
        final Product newProduct = productRepository.findOne(savedProduct.getId());
        assertThat(newProduct.getApprove()).isTrue();
        assertThat(savedProduct.getApprove()).isFalse();
    }

    @Test
    public void shouldReturnNewProductWithUpdateValidFlagSetToFalse() {
        //Given
        final Product savedProduct = productRepository.save(product);

        //When
        productRepository.updateValidFlag(DateTime.parse("2017-06-01").toDate());

        //Then
        final Product newProduct = productRepository.findOne(savedProduct.getId());
        assertThat(newProduct.getValid()).isFalse();
        assertThat(savedProduct.getValid()).isTrue();
    }

    private void defaultAssertForDefaultProductTesting(Product saveProduct) {
        assertThat(saveProduct.getApprove()).isNotNull();
        assertThat(saveProduct.getUserInfo().getId()).isEqualTo(userInfo.getId());
        assertThat(saveProduct)
                .hasFieldOrPropertyWithValue("title", "test_title")
                .hasFieldOrPropertyWithValue("image", null)
                .hasFieldOrPropertyWithValue("location", "test_location")
                .hasFieldOrPropertyWithValue("priceOrigin", BigDecimal.valueOf(20.8))
                .hasFieldOrPropertyWithValue("priceDiscount", BigDecimal.valueOf(10.0))
                .hasFieldOrPropertyWithValue("validDate", DateTime.parse("2017-05-31").toDate())
                .hasFieldOrPropertyWithValue("valid", true)
                .hasFieldOrPropertyWithValue("approve", true)
                .hasFieldOrPropertyWithValue("stock", 5)
                .hasFieldOrPropertyWithValue("deleted", false);
    }

    private void defaultAssertForDefaultUserInfoTesting(UserInfo userInfo) {
        assertThat(userInfo.getId()).isNotNull();
        assertThat(userInfo)
                .hasFieldOrPropertyWithValue("phoneNumber", "189xxxx8888")
                .hasFieldOrPropertyWithValue("account", "mentu")
                .hasFieldOrPropertyWithValue("password", "gsy")
                .hasFieldOrPropertyWithValue("type", NORMAL)
                .hasFieldOrPropertyWithValue("name", null)
                .hasFieldOrPropertyWithValue("identity", null)
                .hasFieldOrPropertyWithValue("address", null)
                .hasFieldOrPropertyWithValue("deleted", false);
    }

}
