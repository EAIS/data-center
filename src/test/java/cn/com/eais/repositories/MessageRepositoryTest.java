package cn.com.eais.repositories;

import cn.com.eais.entities.Message;
import cn.com.eais.entities.Product;
import cn.com.eais.entities.UserInfo;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.List;

import static cn.com.eais.commons.ProductBuilder.CREATE_DEFAULT_PRODUCT;
import static cn.com.eais.commons.UserInfoBuilder.userInfoForJpaTesting;
import static cn.com.eais.commons.UserInfoBuilder.followingUserInfoForJpaTesting;
import static cn.com.eais.enums.UserType.NORMAL;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = NONE)
public class MessageRepositoryTest {

    @Autowired
    private MessageRepository messageRepository;

    @Autowired
    private UserInfoRepository userInfoRepository;

    @Autowired
    private ProductRepository productRepository;

    private UserInfo senderUserInfo;
    private UserInfo receiverUserInfo;
    private Product product;
    private Message message;

    @Before
    public void setUp() throws Exception {
        senderUserInfo = userInfoRepository.save(userInfoForJpaTesting());
        receiverUserInfo = userInfoRepository.save(followingUserInfoForJpaTesting());
        product = productRepository.save(CREATE_DEFAULT_PRODUCT(senderUserInfo));
        message = new Message();
        message.setReceiverUserInfo(receiverUserInfo);
        message.setSenderUserInfo(senderUserInfo);
        message.setProduct(product);
        message.setHasRead(false);
    }

    @Test
    public void shouldReturnMessageAfterSaveIt() {
        //When
        Message savedMessage = messageRepository.save(message);

        //Then
        assertThat(savedMessage.getSenderUserInfo().getId()).isEqualTo(senderUserInfo.getId());
        assertThat(savedMessage.getReceiverUserInfo().getId()).isEqualTo(receiverUserInfo.getId());
        assertThat(savedMessage.getProduct().getId()).isEqualTo(product.getId());
        assertThat(savedMessage.isHasRead()).isEqualTo(false);
    }

    @Test
    public void shouldReturnMessageWhenSelectByReceiver() {
        //Given
        UserInfo tempReceiverUserInfo = new UserInfo();
        tempReceiverUserInfo.setId(receiverUserInfo.getId());
        messageRepository.save(message);

        //When
        List<Message> testMessage = messageRepository.findByReceiverUserInfo(tempReceiverUserInfo);

        //Then
        assertThat(testMessage.get(0).getSenderUserInfo().getId()).isEqualTo(senderUserInfo.getId());
        assertThat(testMessage.get(0).getReceiverUserInfo().getId()).isEqualTo(receiverUserInfo.getId());
        assertThat(testMessage.get(0).getProduct().getId()).isEqualTo(product.getId());
        assertThat(testMessage.get(0).isHasRead()).isEqualTo(false);
        defaultAssertForDefaultUserInfoTesting(testMessage.get(0).getSenderUserInfo());
        defaultAssertForFollowingUserInfoTesting(testMessage.get(0).getReceiverUserInfo());
        defaultAssertForDefaultProductTesting(testMessage.get(0).getProduct(), testMessage.get(0).getSenderUserInfo().getId());
    }

    @Test
    public void shouldReturnMessageWhenSelectBySender() {
        //Given
        UserInfo tempSenderUserInfo = new UserInfo();
        tempSenderUserInfo.setId(senderUserInfo.getId());
        messageRepository.save(message);

        //When
        List<Message> testMessage = messageRepository.findBySenderUserInfo(tempSenderUserInfo);

        //Then
        assertThat(testMessage.get(0).getSenderUserInfo().getId()).isEqualTo(senderUserInfo.getId());
        assertThat(testMessage.get(0).getReceiverUserInfo().getId()).isEqualTo(receiverUserInfo.getId());
        assertThat(testMessage.get(0).getProduct().getId()).isEqualTo(product.getId());
        assertThat(testMessage.get(0).isHasRead()).isEqualTo(false);
        defaultAssertForDefaultUserInfoTesting(testMessage.get(0).getSenderUserInfo());
        defaultAssertForFollowingUserInfoTesting(testMessage.get(0).getReceiverUserInfo());
        defaultAssertForDefaultProductTesting(testMessage.get(0).getProduct(), testMessage.get(0).getSenderUserInfo().getId());
    }

    @Test
    public void shouldReturnAlreadyReadMessageAfterReceiverHasReadIt() {
        //Given
        Message savedMessage = messageRepository.save(message);

        //When
        messageRepository.updateById(savedMessage.getId());
        List<Message> testMessage = messageRepository.findByReceiverUserInfo(savedMessage.getReceiverUserInfo());

        //Then
        assertThat(testMessage.get(0).isHasRead()).isEqualTo(true);
    }

    private void defaultAssertForDefaultUserInfoTesting(UserInfo userInfo) {
        assertThat(userInfo.getId()).isNotNull();
        assertThat(userInfo)
                .hasFieldOrPropertyWithValue("phoneNumber", "189xxxx8888")
                .hasFieldOrPropertyWithValue("account", "mentu")
                .hasFieldOrPropertyWithValue("password", "gsy")
                .hasFieldOrPropertyWithValue("type", NORMAL)
                .hasFieldOrPropertyWithValue("name", null)
                .hasFieldOrPropertyWithValue("identity", null)
                .hasFieldOrPropertyWithValue("address", null)
                .hasFieldOrPropertyWithValue("deleted", false);
    }

    private void defaultAssertForFollowingUserInfoTesting(UserInfo userInfo) {
        assertThat(userInfo.getId()).isNotNull();
        assertThat(userInfo)
                .hasFieldOrPropertyWithValue("phoneNumber", "189xxxx6666")
                .hasFieldOrPropertyWithValue("account", "wangpang")
                .hasFieldOrPropertyWithValue("password", "wh")
                .hasFieldOrPropertyWithValue("type", NORMAL)
                .hasFieldOrPropertyWithValue("name", null)
                .hasFieldOrPropertyWithValue("identity", null)
                .hasFieldOrPropertyWithValue("address", null)
                .hasFieldOrPropertyWithValue("deleted", false);
    }

    private void defaultAssertForDefaultProductTesting(Product saveProduct, Long senderId) {
        assertThat(saveProduct.getApprove()).isNotNull();
        assertThat(saveProduct.getUserInfo().getId()).isEqualTo(senderId);
        assertThat(saveProduct)
                .hasFieldOrPropertyWithValue("title", "test_title")
                .hasFieldOrPropertyWithValue("image", null)
                .hasFieldOrPropertyWithValue("location", "test_location")
                .hasFieldOrPropertyWithValue("priceOrigin", BigDecimal.valueOf(20.8))
                .hasFieldOrPropertyWithValue("priceDiscount", BigDecimal.valueOf(10.0))
                .hasFieldOrPropertyWithValue("validDate", DateTime.parse("2017-05-31").toDate())
                .hasFieldOrPropertyWithValue("valid", true)
                .hasFieldOrPropertyWithValue("approve", true)
                .hasFieldOrPropertyWithValue("stock", 5)
                .hasFieldOrPropertyWithValue("deleted", false);
    }
}
