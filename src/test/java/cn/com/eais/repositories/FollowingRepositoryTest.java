package cn.com.eais.repositories;


import cn.com.eais.entities.Following;
import cn.com.eais.entities.UserInfo;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static cn.com.eais.commons.UserInfoBuilder.userInfoForJpaTesting;
import static cn.com.eais.commons.UserInfoBuilder.followingUserInfoForJpaTesting;
import static cn.com.eais.enums.UserType.NORMAL;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = NONE)
public class FollowingRepositoryTest {

    @Autowired
    private FollowingRepository followingRepository;

    @Autowired
    private UserInfoRepository userInfoRepository;

    private UserInfo userInfo;
    private UserInfo followingUserInfo;
    private Following following;


    @Before
    public void setUp() throws Exception {
        userInfo = userInfoRepository.save(userInfoForJpaTesting());
        followingUserInfo = userInfoRepository.save(followingUserInfoForJpaTesting());
        following = new Following();
        following.setUserInfo(userInfo);
        following.setFollowingUserInfo(followingUserInfo);
    }

    @Test
    public void shouldReturnFollowingAfterSaveIt() {
        //When
        Following savedFollowing = followingRepository.save(following);

        //Then
        assertThat(savedFollowing.getUserInfo().getId()).isEqualTo(userInfo.getId());
        assertThat(savedFollowing.getFollowingUserInfo().getId()).isEqualTo(followingUserInfo.getId());
    }

    @Test
    public void shouldReturnFollowingWhenSelectByUserInfo() {
        //Given
        UserInfo tempUserInfo = new UserInfo();
        tempUserInfo.setId(userInfo.getId());
        followingRepository.save(following);

        //When
        List<Following> testFollowing = followingRepository.findByUserInfo(tempUserInfo);

        //Then
        assertThat(testFollowing.get(0).getUserInfo().getId()).isEqualTo(userInfo.getId());
        assertThat(testFollowing.get(0).getFollowingUserInfo().getId()).isEqualTo(followingUserInfo.getId());
        defaultAssertForDefaultUserInfoTesting(testFollowing.get(0).getUserInfo());
        defaultAssertForFollowingUserInfoTesting(testFollowing.get(0).getFollowingUserInfo());

    }

    @Test
    public void shouldReturnFollowingWhenSelectByFollowingUserInfo() {
        //Given
        UserInfo tempUserInfo = new UserInfo();
        tempUserInfo.setId(followingUserInfo.getId());
        followingRepository.save(following);

        //When
        List<Following> testFollowing = followingRepository.findByFollowingUserInfo(tempUserInfo);

        //Then
        assertThat(testFollowing.get(0).getUserInfo().getId()).isEqualTo(userInfo.getId());
        assertThat(testFollowing.get(0).getFollowingUserInfo().getId()).isEqualTo(followingUserInfo.getId());
        defaultAssertForDefaultUserInfoTesting(testFollowing.get(0).getUserInfo());
        defaultAssertForFollowingUserInfoTesting(testFollowing.get(0).getFollowingUserInfo());

    }

    @Test
    public void shouldReturnNullWhenDeleteByUserInfoAndFollowingUserInfo() {
        //Given
        UserInfo tempFollowingUserInfo = new UserInfo();
        tempFollowingUserInfo.setId(followingUserInfo.getId());
        UserInfo tempUserInfo = new UserInfo();
        tempUserInfo.setId(userInfo.getId());
        followingRepository.save(following);

        //When
        followingRepository.deleteByUserInfoAndFollowingUserInfo(tempUserInfo, tempFollowingUserInfo);

        //Then
        List<Following> tempFollowing = followingRepository.findByUserInfo(tempUserInfo);
        List<Following> tempFollowed = followingRepository.findByUserInfo(tempFollowingUserInfo);
        assertThat(tempFollowing.size()).isEqualTo(0);
        assertThat(tempFollowed.size()).isEqualTo(0);
    }

    private void defaultAssertForDefaultUserInfoTesting(UserInfo userInfo) {
        assertThat(userInfo.getId()).isNotNull();
        assertThat(userInfo)
                .hasFieldOrPropertyWithValue("phoneNumber", "189xxxx8888")
                .hasFieldOrPropertyWithValue("account", "mentu")
                .hasFieldOrPropertyWithValue("password", "gsy")
                .hasFieldOrPropertyWithValue("type", NORMAL)
                .hasFieldOrPropertyWithValue("name", null)
                .hasFieldOrPropertyWithValue("identity", null)
                .hasFieldOrPropertyWithValue("address", null)
                .hasFieldOrPropertyWithValue("deleted", false);
    }

    private void defaultAssertForFollowingUserInfoTesting(UserInfo userInfo) {
        assertThat(userInfo.getId()).isNotNull();
        assertThat(userInfo)
                .hasFieldOrPropertyWithValue("phoneNumber", "189xxxx6666")
                .hasFieldOrPropertyWithValue("account", "wangpang")
                .hasFieldOrPropertyWithValue("password", "wh")
                .hasFieldOrPropertyWithValue("type", NORMAL)
                .hasFieldOrPropertyWithValue("name", null)
                .hasFieldOrPropertyWithValue("identity", null)
                .hasFieldOrPropertyWithValue("address", null)
                .hasFieldOrPropertyWithValue("deleted", false);
    }
}
