package cn.com.eais.repositories;

import cn.com.eais.entities.UserInfo;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static cn.com.eais.commons.UserInfoBuilder.userInfoForJpaTesting;
import static cn.com.eais.enums.UserType.NORMAL;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace= NONE)
public class UserInfoRepositoryTest {

    @Autowired
    private UserInfoRepository repository;
    private UserInfo userInfo;

    @Before
    public void setUp() throws Exception {
        userInfo = userInfoForJpaTesting();
        repository.deleteAll();
    }

    @Test
    public void shouldReturnUserInfoAfterSaveData() {
        //When
        UserInfo savedUser = repository.save(userInfo);

        //Then
        defaultAssertForDefaultUserInfoTesting(savedUser);
    }

    @Test
    public void shouldReturnUserInfoWhenGiveUserId() {
        //Given
        UserInfo savedUser = repository.save(userInfo);

        //When
        UserInfo testUser = repository.findByIdAndDeleted(savedUser.getId(), false);

        //Then
        defaultAssertForDefaultUserInfoTesting(testUser);
    }

    @Test
    public void shouldReturnUserInfoWhenGiveUserPhoneNumber() {
        //Given
        UserInfo savedUser = repository.save(userInfo);

        //When
        UserInfo testUser = repository.findByPhoneNumberAndDeleted(savedUser.getPhoneNumber(), false);

        //Then
        defaultAssertForDefaultUserInfoTesting(testUser);
    }

    @Test
    public void shouldReturnUserInfoWhenGiveUserAccount() {
        //Given
        UserInfo savedUser = repository.save(userInfo);

        //When
        UserInfo testUser = repository.findByAccountAndDeleted(savedUser.getAccount(), false);

        //Then
        defaultAssertForDefaultUserInfoTesting(testUser);
    }

    @Test
    public void shouldReturnUserInfoWhenGiveUserType() {
        //Given
        UserInfo savedUser = repository.save(userInfo);

        //When
        List<UserInfo> testUser = repository.findByTypeAndDeleted(savedUser.getType(), false);

        //Then
        defaultAssertForDefaultUserInfoTesting(testUser.get(0));
    }

    @Test
    public void shouldReturnUserInfoWhenGiveUserValidStatus() {
        //Given
        UserInfo savedUser = repository.save(userInfo);

        //When
        List<UserInfo> testUser = repository.findByDeleted(savedUser.getDeleted());

        //Then
        defaultAssertForDefaultUserInfoTesting(testUser.get(0));
    }

    private void defaultAssertForDefaultUserInfoTesting(UserInfo userInfo) {
        assertThat(userInfo.getId()).isNotNull();
        assertThat(userInfo)
                .hasFieldOrPropertyWithValue("phoneNumber", "189xxxx8888")
                .hasFieldOrPropertyWithValue("account", "mentu")
                .hasFieldOrPropertyWithValue("password", "gsy")
                .hasFieldOrPropertyWithValue("type", NORMAL)
                .hasFieldOrPropertyWithValue("name", null)
                .hasFieldOrPropertyWithValue("identity", null)
                .hasFieldOrPropertyWithValue("address", null)
                .hasFieldOrPropertyWithValue("deleted", false);
    }
}
