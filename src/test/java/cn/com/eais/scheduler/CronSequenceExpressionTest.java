package cn.com.eais.scheduler;

import org.joda.time.DateTime;
import org.junit.Test;
import org.springframework.scheduling.support.CronSequenceGenerator;

import java.util.Date;

public class CronSequenceExpressionTest {

    private String every12Hour = "0 0 0 * * * ";
    private String every15Minutes = "0 */15 * * * *";


    @Test
    public void shouldRunEvery12Hour() {
        //Given
        CronSequenceGenerator generator = new CronSequenceGenerator(every12Hour);

        //When
        Date nextDate = generator.next(DateTime.now().toDate());
        Date nextNextDate = generator.next(nextDate);

        //Then
        System.out.println(nextDate);
        System.out.println(nextNextDate);
    }

    @Test
    public void shouldRunEvery15Minute() {
        //Given
        CronSequenceGenerator generator = new CronSequenceGenerator(every15Minutes);

        //When
        Date nextDate = generator.next(DateTime.now().toDate());
        Date nextNextDate = generator.next(nextDate);

        //Then
        System.out.println(nextDate);
        System.out.println(nextNextDate);
    }
}
