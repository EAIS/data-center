package cn.com.eais.services;

import cn.com.eais.entities.UserInfo;
import cn.com.eais.repositories.UserInfoRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static cn.com.eais.utils.security.SecurityCoding.decrypt;
import static cn.com.eais.utils.security.SecurityCoding.encrypt;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

@RunWith(MockitoJUnitRunner.class)
public class UserInfoServiceTest {

    @Mock
    private UserInfoRepository userInfoRepository;

    private UserInfoService userInfoService;

    @Before
    public void setUp() throws Exception {
        userInfoService = new UserInfoService(userInfoRepository);
    }

    @Test
    public void shouldReturnSavedUserInfoWhenAddUserInfoSuccess() {
        //Given
        UserInfo userInfo = new UserInfo();
        given(userInfoRepository.save(userInfo)).willReturn(userInfo);

        //When
        final UserInfo result = userInfoService.addUserInfo(userInfo);

        //Then
        assertThat(result).isNotNull();
    }

    @Test
    public void shouldReturnFoundUserInfoWhenUserInfoIsCorrectFindByPhone() {
        //Given
        UserInfo requestUserInfo = new UserInfo();
        requestUserInfo.setPhoneNumber("13973738888");
        requestUserInfo.setPassword("123123");
        UserInfo responseUserInfo = new UserInfo();
        responseUserInfo.setPhoneNumber("13973738888");
        responseUserInfo.setPassword(encrypt("123123"));

        given(userInfoRepository.findByPhoneNumberAndDeleted(requestUserInfo.getPhoneNumber(), false)).willReturn(responseUserInfo);

        //When
        final UserInfo result = userInfoService.selectUserInfoByPhoneNumber(requestUserInfo);

        //Then
        assertThat(result).isNotNull();
        assertThat(result.getPhoneNumber()).isEqualTo("13973738888");
    }

    @Test
    public void shouldReturnNullWhenUserInfoIsInCorrectFindByPhone() {
        //Given
        UserInfo requestUserInfo = new UserInfo();
        requestUserInfo.setPhoneNumber("13973738888");
        requestUserInfo.setPassword("123123");

        given(userInfoRepository.findByPhoneNumberAndDeleted(requestUserInfo.getPhoneNumber(), false)).willReturn(null);

        //When
        final UserInfo result = userInfoService.selectUserInfoByPhoneNumber(requestUserInfo);

        //Then
        assertThat(result).isNull();
    }

    @Test
    public void shouldReturnFoundUserInfoWhenUserInfoIsCorrectFindByAccount() {
        //Given
        UserInfo requestUserInfo = new UserInfo();
        requestUserInfo.setAccount("mentu");
        requestUserInfo.setPassword("123123");
        UserInfo responseUserInfo = new UserInfo();
        responseUserInfo.setAccount("mentu");
        responseUserInfo.setPassword(encrypt("123123"));

        given(userInfoRepository.findByAccountAndDeleted(requestUserInfo.getAccount(), false)).willReturn(responseUserInfo);

        //When
        final UserInfo result = userInfoService.selectUserInfoByAccount(requestUserInfo);

        //Then
        assertThat(result).isNotNull();
        assertThat(result.getAccount()).isEqualTo("mentu");
    }

    @Test
    public void shouldReturnNullWhenUserInfoIsInCorrectFindByAccount() {
        //Given
        UserInfo requestUserInfo = new UserInfo();
        requestUserInfo.setAccount("mentu");
        requestUserInfo.setPassword("123123");

        given(userInfoRepository.findByAccountAndDeleted(requestUserInfo.getAccount(), false)).willReturn(null);

        //When
        final UserInfo result = userInfoService.selectUserInfoByAccount(requestUserInfo);

        //Then
        assertThat(result).isNull();
    }

    @Test
    public void shouldReturnNullWhenUserInfoCanNotFoundInDatabase() {
        //Given
        UserInfo[] userInfs = new UserInfo[2];
        userInfs[0] = new UserInfo();
        userInfs[1] = new UserInfo();
        userInfs[0].setId(1L);
        userInfs[0].setPassword("11");
        userInfs[1].setPassword("22");

        given(userInfoRepository.findByIdAndDeleted(userInfs[0].getId(), false)).willReturn(null);

        //When
        final UserInfo result = userInfoService.updateUserPassword(userInfs[0], userInfs[1]);

        //Then
        assertThat(result).isNull();
    }

    @Test
    public void shouldReturnNullWhenUserInfoWithWrongPassword() {
        //Given
        UserInfo[] userInfs = new UserInfo[2];
        userInfs[0] = new UserInfo();
        userInfs[1] = new UserInfo();
        userInfs[0].setId(1L);
        userInfs[0].setPassword("11");
        userInfs[1].setPassword("22");

        UserInfo updatedUserInfo = new UserInfo();
        updatedUserInfo.setId(1L);
        updatedUserInfo.setPassword(encrypt("12"));

        given(userInfoRepository.findByIdAndDeleted(userInfs[0].getId(), false)).willReturn(updatedUserInfo);

        //When
        final UserInfo result = userInfoService.updateUserPassword(userInfs[0], userInfs[1]);

        //Then
        assertThat(result).isNull();
    }

    @Test
    public void shouldReturnUpdatedUserInfoWhenUserInfoIsCorrect() {
        //Given
        UserInfo[] userInfs = new UserInfo[2];
        userInfs[0] = new UserInfo();
        userInfs[1] = new UserInfo();
        userInfs[0].setId(1L);
        userInfs[0].setPassword("11");
        userInfs[1].setPassword("22");

        UserInfo updatedUserInfo = new UserInfo();
        updatedUserInfo.setId(1L);
        updatedUserInfo.setPassword(encrypt("11"));

        given(userInfoRepository.findByIdAndDeleted(userInfs[0].getId(), false)).willReturn(updatedUserInfo);

        //When
        final UserInfo result = userInfoService.updateUserPassword(userInfs[0], userInfs[1]);

        //Then
        assertThat(result).isNotNull();
        assertThat(decrypt(result.getPassword())).isEqualTo("22");
    }
}