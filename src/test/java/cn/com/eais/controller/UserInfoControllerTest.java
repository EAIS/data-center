package cn.com.eais.controller;

import cn.com.eais.entities.UserInfo;
import cn.com.eais.model.ErrorMessage;
import cn.com.eais.services.UserInfoService;
import cn.com.eais.validator.ValidatorContext;
import cn.com.eais.validator.userinfo.UserInfoValidatorFacade;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.util.Lists.newArrayList;
import static org.mockito.BDDMockito.given;
import static org.springframework.http.HttpStatus.*;

@RunWith(MockitoJUnitRunner.class)
public class UserInfoControllerTest {

    @Mock
    private UserInfoService userInfoService;

    @Mock
    private UserInfoValidatorFacade userInfoValidatorFacade;

    @Mock
    private ValidatorContext<UserInfo> validatorContext;

    private UserInfoController userInfoController;

    @Before
    public void setUp() throws Exception {
        userInfoController = new UserInfoController(userInfoValidatorFacade, userInfoService);
    }

    @Test
    public void shouldReturnCreatedWhenRegistrySuccess() {
        //Given
        UserInfo userInfo = new UserInfo();
        given(userInfoValidatorFacade.registryUserInfoValidator()).willReturn(validatorContext);
        given(validatorContext.validate(userInfo)).willReturn(newArrayList());
        given(userInfoService.addUserInfo(userInfo)).willReturn(userInfo);

        //When
        ResponseEntity result = userInfoController.registryUserInfo(userInfo);

        //Then
        assertThat(result.getStatusCode()).isEqualTo(OK);
    }

    @Test
    public void shouldReturnBadRequestWhenValidationFailedDuringRegistry() {
        //Given
        UserInfo userInfo = new UserInfo();
        given(userInfoValidatorFacade.registryUserInfoValidator()).willReturn(validatorContext);
        final List<ErrorMessage> errors = newArrayList();
        errors.add(new ErrorMessage("test", "test"));
        given(validatorContext.validate(userInfo)).willReturn(errors);

        //When
        ResponseEntity<?> result =  userInfoController.registryUserInfo(userInfo);

        //Then
        assertThat(result.getStatusCode()).isEqualTo(BAD_REQUEST);
    }

    @Test
    public void shouldReturnFoundUserInfoWhenLoginPhoneSuccess() {
        //Given
        UserInfo userInfo = new UserInfo();
        given(userInfoValidatorFacade.loginPhoneUserInfoValidator()).willReturn(validatorContext);
        given(validatorContext.validate(userInfo)).willReturn(newArrayList());
        given(userInfoService.selectUserInfoByPhoneNumber(userInfo)).willReturn(userInfo);

        //When
        ResponseEntity result = userInfoController.loginPhone(userInfo);

        //Then
        assertThat(result.getStatusCode()).isEqualTo(OK);
    }

    @Test
    public void shouldReturnBadRequestWhenValidationFailedDuringLoginPhone() {
        //Given
        UserInfo userInfo = new UserInfo();
        given(userInfoValidatorFacade.loginPhoneUserInfoValidator()).willReturn(validatorContext);
        final List<ErrorMessage> errors = newArrayList();
        errors.add(new ErrorMessage("test", "test"));
        given(validatorContext.validate(userInfo)).willReturn(errors);

        //When
        ResponseEntity<?> result =  userInfoController.loginPhone(userInfo);

        //Then
        assertThat(result.getStatusCode()).isEqualTo(BAD_REQUEST);
    }

    @Test
    public void shouldReturnNotFoundWhenLoginPhone() {
        //Given
        UserInfo userInfo = new UserInfo();
        given(userInfoValidatorFacade.loginPhoneUserInfoValidator()).willReturn(validatorContext);
        given(validatorContext.validate(userInfo)).willReturn(newArrayList());
        given(userInfoService.selectUserInfoByPhoneNumber(userInfo)).willReturn(null);

        //When
        ResponseEntity result = userInfoController.loginPhone(userInfo);

        //Then
        assertThat(result.getStatusCode()).isEqualTo(NOT_FOUND);
    }

    @Test
    public void shouldReturnFoundUserInfoWhenLoginAccountSuccess() {
        //Given
        UserInfo userInfo = new UserInfo();
        given(userInfoValidatorFacade.loginAccountUserInfoValidator()).willReturn(validatorContext);
        given(validatorContext.validate(userInfo)).willReturn(newArrayList());
        given(userInfoService.selectUserInfoByAccount(userInfo)).willReturn(userInfo);

        //When
        ResponseEntity result = userInfoController.loginAccount(userInfo);

        //Then
        assertThat(result.getStatusCode()).isEqualTo(OK);
    }

    @Test
    public void shouldReturnBadRequestWhenValidationFailedDuringLoginAccount() {
        //Given
        UserInfo userInfo = new UserInfo();
        given(userInfoValidatorFacade.loginAccountUserInfoValidator()).willReturn(validatorContext);
        final List<ErrorMessage> errors = newArrayList();
        errors.add(new ErrorMessage("test", "test"));
        given(validatorContext.validate(userInfo)).willReturn(errors);

        //When
        ResponseEntity<?> result =  userInfoController.loginAccount(userInfo);

        //Then
        assertThat(result.getStatusCode()).isEqualTo(BAD_REQUEST);
    }

    @Test
    public void shouldReturnNotFoundWhenLoginAccount() {
        //Given
        UserInfo userInfo = new UserInfo();
        given(userInfoValidatorFacade.loginAccountUserInfoValidator()).willReturn(validatorContext);
        given(validatorContext.validate(userInfo)).willReturn(newArrayList());
        given(userInfoService.selectUserInfoByAccount(userInfo)).willReturn(null);

        //When
        ResponseEntity result = userInfoController.loginAccount(userInfo);

        //Then
        assertThat(result.getStatusCode()).isEqualTo(NOT_FOUND);
    }

    @Test
    public void shouldReturnNotFoundWhenSearchById() {
        //Given
        Long id = 1L;
        given(userInfoService.findUserInfoById(id)).willReturn(null);

        //When
        ResponseEntity result = userInfoController.searchById(id);

        //Then
        assertThat(result.getStatusCode()).isEqualTo(NOT_FOUND);
    }

    @Test
    public void shouldReturnOKWhenSearchById() {
        //Given
        Long id = 1L;
        given(userInfoService.findUserInfoById(id)).willReturn(new UserInfo());

        //When
        ResponseEntity result = userInfoController.searchById(id);

        //Then
        assertThat(result.getStatusCode()).isEqualTo(OK);
    }

    @Test
    public void shouldReturnBadRequestWhenUpdateDetails() {
        UserInfo userInfo = new UserInfo();
        given(userInfoValidatorFacade.updateUserDetailsValidator()).willReturn(validatorContext);
        final List<ErrorMessage> errors = newArrayList();
        errors.add(new ErrorMessage("test", "test"));
        given(validatorContext.validate(userInfo)).willReturn(errors);

        //When
        ResponseEntity<?> result =  userInfoController.updateUserDetails(userInfo);

        //Then
        assertThat(result.getStatusCode()).isEqualTo(BAD_REQUEST);
    }

    @Test
    public void shouldReturnNotFoundWhenUpdateDetails() {
        //Given
        final UserInfo userInfo = new UserInfo();
        given(userInfoValidatorFacade.updateUserDetailsValidator()).willReturn(validatorContext);
        given(validatorContext.validate(userInfo)).willReturn(newArrayList());
        given(userInfoService.updateUserDetails(userInfo)).willReturn(null);

        //When
        final ResponseEntity result = userInfoController.updateUserDetails(userInfo);

        //Then
        assertThat(result.getStatusCode()).isEqualTo(NOT_FOUND);

    }

    @Test
    public void shouldReturnOkWhenUpdateDetails() {
        //Given
        final UserInfo userInfo = new UserInfo();
        given(userInfoValidatorFacade.updateUserDetailsValidator()).willReturn(validatorContext);
        given(validatorContext.validate(userInfo)).willReturn(newArrayList());
        given(userInfoService.updateUserDetails(userInfo)).willReturn(userInfo);

        //When
        final ResponseEntity result = userInfoController.updateUserDetails(userInfo);

        //Then
        assertThat(result.getStatusCode()).isEqualTo(OK);

    }

    @Test
    public void shouldReturnBadRequestWhenUpdatePhone() {
        UserInfo userInfo = new UserInfo();
        given(userInfoValidatorFacade.updatePhoneValidator()).willReturn(validatorContext);
        final List<ErrorMessage> errors = newArrayList();
        errors.add(new ErrorMessage("test", "test"));
        given(validatorContext.validate(userInfo)).willReturn(errors);

        //When
        ResponseEntity<?> result =  userInfoController.updatePhoneNumber(userInfo);

        //Then
        assertThat(result.getStatusCode()).isEqualTo(BAD_REQUEST);
    }

    @Test
    public void shouldReturnNotFoundWhenUpdatePhone() {
        //Given
        final UserInfo userInfo = new UserInfo();
        given(userInfoValidatorFacade.updatePhoneValidator()).willReturn(validatorContext);
        given(validatorContext.validate(userInfo)).willReturn(newArrayList());
        given(userInfoService.updateUserPhone(userInfo)).willReturn(null);

        //When
        final ResponseEntity result = userInfoController.updatePhoneNumber(userInfo);

        //Then
        assertThat(result.getStatusCode()).isEqualTo(NOT_FOUND);

    }

    @Test
    public void shouldReturnNoContentWhenUpdatePhone() {
        //Given
        final UserInfo userInfo = new UserInfo();
        given(userInfoValidatorFacade.updatePhoneValidator()).willReturn(validatorContext);
        given(validatorContext.validate(userInfo)).willReturn(newArrayList());
        given(userInfoService.updateUserPhone(userInfo)).willReturn(userInfo);

        //When
        final ResponseEntity result = userInfoController.updatePhoneNumber(userInfo);

        //Then
        assertThat(result.getStatusCode()).isEqualTo(NO_CONTENT);

    }

    @Test
    public void shouldReturnBadRequestWhenUpdatePassword() {
        UserInfo[] userInfo = new UserInfo[2];
        userInfo[0] = new UserInfo();
        userInfo[1] = new UserInfo();
        given(userInfoValidatorFacade.updatePasswordValidator()).willReturn(validatorContext);
        final List<ErrorMessage> errors = newArrayList();
        errors.add(new ErrorMessage("test", "test"));
        given(validatorContext.validate(userInfo[0])).willReturn(errors);
        given(validatorContext.validate(userInfo[1])).willReturn(errors);

        //When
        ResponseEntity<?> result =  userInfoController.updatePassword(userInfo);

        //Then
        assertThat(result.getStatusCode()).isEqualTo(BAD_REQUEST);
    }

    @Test
    public void shouldReturnNotFoundWhenUpdatePassword() {
        //Given
        UserInfo[] userInfo = new UserInfo[2];
        userInfo[0] = new UserInfo();
        userInfo[1] = new UserInfo();
        given(userInfoValidatorFacade.updatePasswordValidator()).willReturn(validatorContext);
        given(validatorContext.validate(userInfo[0])).willReturn(newArrayList());
        given(validatorContext.validate(userInfo[1])).willReturn(newArrayList());
        given(userInfoService.updateUserPassword(userInfo[0], userInfo[1])).willReturn(null);

        //When
        final ResponseEntity result = userInfoController.updatePassword(userInfo);

        //Then
        assertThat(result.getStatusCode()).isEqualTo(NOT_FOUND);

    }

    @Test
    public void shouldReturnNoContentWhenUpdatePassword() {
        //Given
        UserInfo[] userInfo = new UserInfo[2];
        userInfo[0] = new UserInfo();
        userInfo[1] = new UserInfo();
        given(userInfoValidatorFacade.updatePasswordValidator()).willReturn(validatorContext);
        given(validatorContext.validate(userInfo[0])).willReturn(newArrayList());
        given(validatorContext.validate(userInfo[1])).willReturn(newArrayList());
        given(userInfoService.updateUserPassword(userInfo[0], userInfo[1])).willReturn(userInfo[0]);

        //When
        final ResponseEntity result = userInfoController.updatePassword(userInfo);

        //Then
        assertThat(result.getStatusCode()).isEqualTo(NO_CONTENT);

    }

    @Test
    public void shouldReturnNotFoundWhenDeleteUserNotFoundInDatabase() {
        //Given
        UserInfo userInfo = new UserInfo();
        userInfo.setId(1L);
        given(userInfoService.deleteUserBy(userInfo.getId())).willReturn(null);

        //When
        final ResponseEntity result = userInfoController.deleteUser(userInfo);

        //Then
        assertThat(result.getStatusCode()).isEqualTo(NOT_FOUND);

    }

    @Test
    public void shouldReturnNoContentWhenDeleteUserSuccess() {
        //Given
        UserInfo userInfo = new UserInfo();
        userInfo.setId(1L);
        given(userInfoService.deleteUserBy(userInfo.getId())).willReturn(userInfo);

        //When
        final ResponseEntity result = userInfoController.deleteUser(userInfo);

        //Then
        assertThat(result.getStatusCode()).isEqualTo(NO_CONTENT);

    }


}