package cn.com.eais.controller;

import cn.com.eais.entities.Product;
import cn.com.eais.entities.UserInfo;
import cn.com.eais.model.ErrorMessage;
import cn.com.eais.services.ProductService;
import cn.com.eais.validator.ValidatorContext;
import cn.com.eais.validator.product.ProductValidatorFacade;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;

import static cn.com.eais.commons.ProductBuilder.CREATE_DEFAULT_PRODUCT;
import static com.google.common.collect.Lists.newArrayList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.springframework.http.HttpStatus.*;


@RunWith(MockitoJUnitRunner.class)
public class ProductControllerTest {

    @Mock
    private ProductService productService;

    @Mock
    private ProductValidatorFacade productValidatorFacade;

    @Mock
    private ValidatorContext<Product> validatorContext;

    private ProductController productController;
    private Product product;
    private UserInfo userInfo;
    private String host;

    @Before
    public void setUp() throws Exception {
        userInfo = new UserInfo();
        userInfo.setId(1L);
        product = CREATE_DEFAULT_PRODUCT(userInfo);
        host = "test";
        productController = new ProductController(productService, productValidatorFacade, host);
    }

    @Test
    public void shouldReturnOkWhenAddProductSuccess() {
        //Given
        given(productService.addNewProduct(product)).willReturn(product);
        given(productValidatorFacade.newProductValidator()).willReturn(validatorContext);
        given(validatorContext.validate(product)).willReturn(newArrayList());

        //When
        final ResponseEntity<?> results = productController.addProduct(product);

        //Then
        assertThat(results.getStatusCode()).isEqualTo(OK);
    }

    @Test
    public void shouldReturnBadRequestWhenAddProductHasValidationError() {
        //Given
        given(productValidatorFacade.newProductValidator()).willReturn(validatorContext);
        final ArrayList<ErrorMessage> errorMessages = newArrayList();
        errorMessages.add(new ErrorMessage("field", "message"));
        given(validatorContext.validate(product)).willReturn(errorMessages);

        //When
        final ResponseEntity<?> results = productController.addProduct(product);

        //Then
        assertThat(results.getStatusCode()).isEqualTo(BAD_REQUEST);
    }

    @Test
    public void shouldReturnOkWhenFoundProductsSuccess() {
        //Given
        final ArrayList<Product> products = newArrayList();
        products.add(new Product());
        given(productService.findProducts()).willReturn(products);

        //When
        final ResponseEntity<?> results = productController.getProducts();

        //Then
        assertThat(results.getStatusCode()).isEqualTo(OK);
    }

    @Test
    public void shouldReturnNotFoundWhenFoundProductsFailure() {
        //Given
        given(productService.findProducts()).willReturn(null);

        //When
        final ResponseEntity<?> results = productController.getProducts();

        //Then
        assertThat(results.getStatusCode()).isEqualTo(NOT_FOUND);
    }

    @Test
    public void shouldReturnOkWhenFoundProductsByUserIdSuccess() {
        //Given
        final ArrayList<Product> products = newArrayList();
        products.add(new Product());
        final UserInfo userInfo = new UserInfo();
        userInfo.setId(1L);
        given(productService.findProductsByUserId(userInfo)).willReturn(products);

        //When
        final ResponseEntity<?> results = productController.getProductsByUserId(userInfo);

        //Then
        assertThat(results.getStatusCode()).isEqualTo(OK);
    }

    @Test
    public void shouldReturnNotFoundWhenFoundProductsByUserIdFailure() {
        //Given
        final UserInfo userInfo = new UserInfo();
        userInfo.setId(1L);
        given(productService.findProductsByUserId(userInfo)).willReturn(null);

        //When
        final ResponseEntity<?> results = productController.getProductsByUserId(userInfo);

        //Then
        assertThat(results.getStatusCode()).isEqualTo(NOT_FOUND);
    }

    @Test
    public void shouldReturnOkWhenFoundProductsByTitleSuccess() {
        //Given
        final ArrayList<Product> products = newArrayList();
        products.add(new Product());
        String title = "test";
        given(productService.findProductsByTitle(title)).willReturn(products);

        //When
        final ResponseEntity<?> results = productController.getProductsByTitle(title);

        //Then
        assertThat(results.getStatusCode()).isEqualTo(OK);
    }

    @Test
    public void shouldReturnNotFoundWhenFoundProductsByTitleFailure() {
        //Given
        String title = "test";
        given(productService.findProductsByTitle(title)).willReturn(null);

        //When
        final ResponseEntity<?> results = productController.getProductsByTitle(title);

        //Then
        assertThat(results.getStatusCode()).isEqualTo(NOT_FOUND);
    }

    @Test
    public void shouldReturnOkWhenFoundProductsByLocationSuccess() {
        //Given
        final ArrayList<Product> products = newArrayList();
        products.add(new Product());
        String location = "test";
        given(productService.findProductsByLocation(location)).willReturn(products);

        //When
        final ResponseEntity<?> results = productController.getProductsByLocation(location);

        //Then
        assertThat(results.getStatusCode()).isEqualTo(OK);
    }

    @Test
    public void shouldReturnNotFoundWhenFoundProductsByLocationFailure() {
        //Given
        String location = "test";
        given(productService.findProductsByLocation(location)).willReturn(null);

        //When
        final ResponseEntity<?> results = productController.getProductsByLocation(location);

        //Then
        assertThat(results.getStatusCode()).isEqualTo(NOT_FOUND);
    }

    @Test
    public void shouldReturnOkWhenFoundProductsByPricesSuccess() {
        //Given
        final ArrayList<Product> products = newArrayList();
        products.add(new Product());
        BigDecimal beginPrice = BigDecimal.valueOf(12.1);
        BigDecimal endPrice = BigDecimal.valueOf(12.5);
        given(productService.findProductsByPrices(beginPrice, endPrice)).willReturn(products);

        //When
        final ResponseEntity<?> results = productController.getProductsByPrices(beginPrice, endPrice);

        //Then
        assertThat(results.getStatusCode()).isEqualTo(OK);
    }

    @Test
    public void shouldReturnNotFoundWhenFoundProductsByPricesFailure() {
        //Given
        BigDecimal beginPrice = BigDecimal.valueOf(12.1);
        BigDecimal endPrice = BigDecimal.valueOf(12.5);
        given(productService.findProductsByPrices(beginPrice, endPrice)).willReturn(null);

        //When
        final ResponseEntity<?> results = productController.getProductsByPrices(beginPrice, endPrice);

        //Then
        assertThat(results.getStatusCode()).isEqualTo(NOT_FOUND);
    }

    @Test
    public void shouldReturnOkWhenFoundProductsByDateSuccess() {
        //Given
        final ArrayList<Product> products = newArrayList();
        products.add(new Product());
        Date validDate = DateTime.parse("2017-10-12").toDate();
        given(productService.findProductsByDate(validDate)).willReturn(products);

        //When
        final ResponseEntity<?> results = productController.getProductsByValidDate(validDate);

        //Then
        assertThat(results.getStatusCode()).isEqualTo(OK);
    }

    @Test
    public void shouldReturnNotFoundWhenFoundProductsByDateFailure() {
        //Given
        Date validDate = DateTime.parse("2017-10-12").toDate();
        given(productService.findProductsByDate(validDate)).willReturn(null);

        //When
        final ResponseEntity<?> results = productController.getProductsByValidDate(validDate);

        //Then
        assertThat(results.getStatusCode()).isEqualTo(NOT_FOUND);
    }

    @Test
    public void shouldReturnOkWhenUpdateProductSuccess() {
        //Given
        given(productService.updateProduct(product)).willReturn(product);
        given(productValidatorFacade.newProductValidator()).willReturn(validatorContext);
        given(validatorContext.validate(product)).willReturn(newArrayList());

        //When
        final ResponseEntity<?> results = productController.updateProduct(product);

        //Then
        assertThat(results.getStatusCode()).isEqualTo(OK);
    }

    @Test
    public void shouldReturnBadRequestWhenUpdateProductHasValidationError() {
        //Given
        given(productValidatorFacade.newProductValidator()).willReturn(validatorContext);
        final ArrayList<ErrorMessage> errorMessages = newArrayList();
        errorMessages.add(new ErrorMessage("field", "message"));
        given(validatorContext.validate(product)).willReturn(errorMessages);

        //When
        final ResponseEntity<?> results = productController.updateProduct(product);

        //Then
        assertThat(results.getStatusCode()).isEqualTo(BAD_REQUEST);
    }

    @Test
    public void shouldReturnNotFoundWhenUpdateProductNotExist() {
        //Given
        given(productService.updateProduct(product)).willReturn(null);
        given(productValidatorFacade.newProductValidator()).willReturn(validatorContext);
        given(validatorContext.validate(product)).willReturn(newArrayList());

        //When
        final ResponseEntity<?> results = productController.updateProduct(product);

        //Then
        assertThat(results.getStatusCode()).isEqualTo(NOT_FOUND);
    }

    @Test
    public void shouldReturnNoContentWhenDeleteProductSuccess() {
        //Given
        product.setId(1L);
        given(productService.deleteProductById(1L)).willReturn(product);

        //When
        final ResponseEntity<?> results = productController.deleteProduct(product);

        //Then
        assertThat(results.getStatusCode()).isEqualTo(NO_CONTENT);
    }

    @Test
    public void shouldReturnNotFoundWhenDeleteProductNotExist() {
        //Given
        product.setId(1L);
        given(productService.deleteProductById(1L)).willReturn(null);

        //When
        final ResponseEntity<?> results = productController.deleteProduct(product);

        //Then
        assertThat(results.getStatusCode()).isEqualTo(NOT_FOUND);
    }
}