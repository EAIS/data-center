package cn.com.eais.commons;

import cn.com.eais.model.RequestHeaders;

public class HttpHeadersBuilder {

    public static RequestHeaders defaultRequestHeaders(String xCorrelationId, String xClientName, String xClientVersion, String xClientKey) {
        return new RequestHeaders(xCorrelationId, xClientName, xClientVersion, xClientKey);
    }

    private HttpHeadersBuilder () {

    }
}
