package cn.com.eais.commons;

import cn.com.eais.entities.UserInfo;

import static cn.com.eais.enums.UserType.NORMAL;

public final class UserInfoBuilder {

    public static UserInfo userInfoForJpaTesting() {
        UserInfo userinfo = new UserInfo();
        userinfo.setPhoneNumber("189xxxx8888");
        userinfo.setAccount("mentu");
        userinfo.setPassword("gsy");
        userinfo.setType(NORMAL);
        userinfo.setDeleted(false);
        return userinfo;
    }

    public static UserInfo followingUserInfoForJpaTesting() {
        UserInfo userinfo = new UserInfo();
        userinfo.setPhoneNumber("189xxxx6666");
        userinfo.setAccount("wangpang");
        userinfo.setPassword("wh");
        userinfo.setType(NORMAL);
        userinfo.setDeleted(false);
        return userinfo;
    }

    public static UserInfo defaultValidUserInfo() {
        final UserInfo userInfo = new UserInfo("18911111919", "hello", "111111", "aaa", "430922199901010039", "hunansheng", NORMAL);
        userInfo.setDeleted(false);
        return userInfo;
    }

    private UserInfoBuilder() {

    }
}
