package cn.com.eais.commons;

import cn.com.eais.entities.Product;
import cn.com.eais.entities.UserInfo;
import org.joda.time.DateTime;

import java.math.BigDecimal;

public final class ProductBuilder {

    public static Product CREATE_DEFAULT_PRODUCT(UserInfo userInfo) {
        Product product = new Product();
        product.setUserInfo(userInfo);
        product.setTitle("test_title");
        product.setLocation("test_location");
        product.setPriceOrigin(BigDecimal.valueOf(20.8));
        product.setPriceDiscount(BigDecimal.valueOf(10.0));
        product.setValidDate(DateTime.parse("2017-05-31").toDate());
        product.setValid(true);
        product.setApprove(true);
        product.setStock(5);
        product.setDeleted(false);
        return product;
    }


    private ProductBuilder() {

    }
}
