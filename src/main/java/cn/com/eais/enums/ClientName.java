package cn.com.eais.enums;

import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

public enum ClientName {
    MIDDLE_LAYER("middleLayer");

    private String value;

    ClientName(String value) {
        this.value = value;
    }

    public static Set<String> getClientNames() {
        return Arrays.stream(values()).map(ClientName::getValue).collect(Collectors.toSet());
    }

    public String getValue() {
        return value;
    }
}
