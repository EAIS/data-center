package cn.com.eais.contants;

public final class Environments {
    public static final String LOCAL = "local";
    public static final String TEST = "test";
    public static final String QA = "qa";

    private Environments() {
    }
}
