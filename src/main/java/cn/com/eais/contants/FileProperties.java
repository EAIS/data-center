package cn.com.eais.contants;

import java.util.HashMap;
import java.util.Map;

public final class FileProperties {

    public static Map<String, String> MIMETYPE_TO_EXTENSION_MAPPER = new HashMap<String, String>() {{
        put("image/bmp", ".bmp");
        put("image/gif", ".gif");
        put("image/x-icon", ".icon");
        put("image/pipeg", ".jfif");
        put("image/jpeg", ".jpg");
        put("image/svg+xml", ".svg");
        put("image/tiff", ".tiff");
    }};

    public static String IMAGE_FILE_FIELD = "product.image";
    public static long MAX_IMAGE_FILE_SIZE = 2 * 1024 * 1024;


    private FileProperties() {

    }
}
