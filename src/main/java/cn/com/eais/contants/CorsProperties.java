package cn.com.eais.contants;

public final class CorsProperties {


    public static final String X_CORRELATION_ID = "X-Correlation-ID";
    public static final String X_CLIENT_NAME = "X-Client-Name";
    public static final String X_CLIENT_VERSION = "X-Client-Version";
    public static final String X_CLIENT_KEY = "X-Client-Key";

    public static final String APPLICATION_JSON_API_VALUE = "application/vnd.api+json";
    public static final String ACCEPT_APPLICATION_JSON_API_VALUE = "Accept=" + APPLICATION_JSON_API_VALUE;


    public static final String X_CORRELATION_ID_EMPTY_ERROR = "xCorrelationId can not empty!";
    public static final String X_CLIENT_NAME_EMPTY_ERROR = "xClientName can not empty!";
    public static final String X_CLIENT_NAME_MATCH_ERROR = "xClientName did not match!";
    public static final String X_CLIENT_VERSION_EMPTY_ERROR = "xClientVersion can not empty!";
    public static final String X_CLIENT_KEY_EMPTY_ERROR = "xClientKey can not empty!";
    public static final String X_CLIENT_KEY_MATCH_ERROR = "xClientKey did not match!";

    private CorsProperties() {

    }
}
