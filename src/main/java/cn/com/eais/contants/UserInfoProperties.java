package cn.com.eais.contants;

public final class UserInfoProperties {

    public static final String PHONE_NUMBER_FIELD = "userinfo.phonenumber";
    public static final String PASSWORD_FIELD = "userinfo.password";
    public static final String ACCOUNT_FIELD = "userinfo.account";
    public static final String NAME_FIELD = "userinfo.name";
    public static final String IDENTITY_FIELD = "userinfo.identity";
    public static final String ADDRESS_FIELD = "userinfo.address";
    public static final String TYPE_FIELD = "userinfo.type";

    private UserInfoProperties() {
    }
}
