package cn.com.eais.contants;

public final class ErrorsProperties {

    public static final String EMPTY_ERROR = "field is empty";
    public static final String ILLEGAL_ERROR = "field is illegal";

    private ErrorsProperties() {
    }
}
