package cn.com.eais.contants;

public final class ProductProperties {

    public static final String USER_ID_FIELD = "product.user.id";
    public static final String TITLE_FIELD = "product.title";
    public static final String LOCATION_FIELD = "product.location";
    public static final String ORIGIN_PRICE_FIELD = "product.price.origin";
    public static final String DISCOUNT_PRICE_FIELD = "product.price.discount";
    public static final String VALID_DATE_FIELD = "product.valid.date";
    public static final String STOCK_FIELD = "product.stock";


    private ProductProperties() {
    }
}
