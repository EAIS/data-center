package cn.com.eais.services;

import cn.com.eais.entities.UserInfo;
import cn.com.eais.repositories.UserInfoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static cn.com.eais.utils.security.SecurityCoding.decrypt;
import static cn.com.eais.utils.security.SecurityCoding.encrypt;

@Service
public class UserInfoService {

    private UserInfoRepository userInfoRepository;

    @Autowired
    public UserInfoService(UserInfoRepository userInfoRepository) {
        this.userInfoRepository = userInfoRepository;
    }

    public UserInfo addUserInfo(UserInfo userInfo) {
        userInfo.setPassword(encrypt(userInfo.getPassword()));
        return userInfoRepository.save(userInfo);
    }

    public UserInfo selectUserInfoByPhoneNumber(UserInfo userInfo) {
        final UserInfo foundUserInfo = userInfoRepository.findByPhoneNumberAndDeleted(userInfo.getPhoneNumber(), false);
        if (foundUserInfo != null && userInfo.getPassword().equals(decrypt(foundUserInfo.getPassword()))) {
            return foundUserInfo;
        }
        return null;
    }

    public UserInfo selectUserInfoByAccount(UserInfo userInfo) {
        final UserInfo foundUserInfo = userInfoRepository.findByAccountAndDeleted(userInfo.getAccount(), false);
        if (foundUserInfo != null && userInfo.getPassword().equals(decrypt(foundUserInfo.getPassword()))) {
            return foundUserInfo;
        }
        return null;
    }

    public UserInfo findUserInfoById(Long id) {
        return userInfoRepository.findByIdAndDeleted(id, false);
    }

    public UserInfo updateUserDetails(UserInfo userInfo) {
        UserInfo updateUserInfo = findUserInfoById(userInfo.getId());
        if (updateUserInfo == null) {
            return null;
        }
        updateUserInfo.setAccount(userInfo.getAccount());
        updateUserInfo.setName(userInfo.getName());
        updateUserInfo.setIdentity(userInfo.getIdentity());
        updateUserInfo.setAddress(userInfo.getAddress());
        updateUserInfo.setType(userInfo.getType());
        userInfoRepository.flush();
        return updateUserInfo;
    }

    public UserInfo updateUserPhone(UserInfo userInfo) {
        UserInfo updateUserInfo = findUserInfoById(userInfo.getId());
        if (updateUserInfo == null) {
            return null;
        }
        updateUserInfo.setPhoneNumber(userInfo.getPhoneNumber());
        userInfoRepository.flush();
        return updateUserInfo;
    }

    public UserInfo updateUserPassword(UserInfo oldUserInfo, UserInfo newUserInfo) {
        UserInfo updatedUserInfo = findUserInfoById(oldUserInfo.getId());
        if (updatedUserInfo == null || !oldUserInfo.getPassword().equals(decrypt(updatedUserInfo.getPassword()))) {
            return null;
        }
        updatedUserInfo.setPassword(encrypt(newUserInfo.getPassword()));
        userInfoRepository.flush();
        return updatedUserInfo;
    }

    public UserInfo deleteUserBy(Long id) {
        final UserInfo updateUserInfo = findUserInfoById(id);
        if (updateUserInfo == null) {
            return null;
        }
        updateUserInfo.setDeleted(true);
        userInfoRepository.flush();
        return updateUserInfo;
    }
}
