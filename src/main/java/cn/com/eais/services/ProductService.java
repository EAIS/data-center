package cn.com.eais.services;

import cn.com.eais.entities.Product;
import cn.com.eais.entities.UserInfo;
import cn.com.eais.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Service
public class ProductService {

    private ProductRepository productRepository;

    @Autowired
    public ProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public Product addNewProduct(Product product) {
        return productRepository.save(product);
    }

    public List<Product> findProducts() {
        return productRepository.findByValidAndApproveAndDeleted(true, true, false);
    }

    public List<Product> findProductsByUserId(UserInfo userInfo) {
        return productRepository.findByUserInfoAndValidAndApproveAndDeleted(userInfo, true, true, false);
    }

    public List<Product> findProductsByTitle(String title) {
        return productRepository.findByTitleContainingAndValidAndApproveAndDeleted(title, true, true, false);
    }

    public List<Product> findProductsByLocation(String location) {
        return productRepository.findByLocationContainingAndValidAndApproveAndDeleted(location, true, true, false);
    }

    public List<Product> findProductsByPrices(BigDecimal beginPrice, BigDecimal endPrice) {
        return productRepository.findByPriceDiscountBetweenAndValidAndApproveAndDeleted(beginPrice, endPrice, true, true, false);
    }

    public List<Product> findProductsByDate(Date validDate) {
        return productRepository.findByValidDateLessThanEqualAndValidAndApproveAndDeleted(validDate, true, true, false);
    }

    public Product updateProduct(Product product) {
        final Product existProduct = productRepository.findOne(product.getId());
        if (existProduct == null) {
            return null;
        }
        existProduct.setTitle(product.getTitle());
        existProduct.setImage(product.getImage());
        existProduct.setLocation(product.getLocation());
        existProduct.setPriceOrigin(product.getPriceOrigin());
        existProduct.setPriceDiscount(product.getPriceDiscount());
        existProduct.setValidDate(product.getValidDate());
        existProduct.setStock(product.getStock());
        productRepository.flush();
        return existProduct;
    }

    public Product deleteProductById(Long id) {
        final Product existProduct = productRepository.findOne(id);
        if (existProduct == null) {
            return null;
        }
        existProduct.setDeleted(true);
        productRepository.flush();
        return existProduct;
    }

    public void updateApproveFlag() {
        productRepository.updateApproveFlag();
    }

    public void updateValidFlag(Date validDate) {
        productRepository.updateValidFlag(validDate);
    }
}
