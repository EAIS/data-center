package cn.com.eais.entities;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "following")
public class Following {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "f_id")
    private Long id;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "u_id")
    private UserInfo userInfo;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "u_follower_id")
    private UserInfo followingUserInfo;

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public void setFollowingUserInfo(UserInfo followingUserInfo) {
        this.followingUserInfo = followingUserInfo;
    }

    public UserInfo getFollowingUserInfo() {
        return followingUserInfo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
