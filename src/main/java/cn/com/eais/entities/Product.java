package cn.com.eais.entities;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;
import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "product")
@ApiModel
@JsonInclude(NON_NULL)
public class Product {

    @Id
    @Column(name = "p_id")
    @GeneratedValue(strategy = IDENTITY)
    @ApiModelProperty(hidden = true)
    private Long id;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "u_id")
    @ApiModelProperty(name = "userInfo.id", required = true)
    private UserInfo userInfo;

    @NotNull
    @Column(name = "p_title", length = 100)
    @ApiModelProperty(name = "product title", required = true)
    private String title;

    @Column(name = "p_image", unique = true, length = 100)
    @ApiModelProperty(name = "product image", required = true)
    private String image;

    @NotNull
    @Column(name = "p_location", length = 100)
    @ApiModelProperty(name = "product location", required = true)
    private String location;

    @NotNull
    @Column(name = "p_price_origin")
    @ApiModelProperty(name = "product original price", required = true)
    private BigDecimal priceOrigin;

    @NotNull
    @Column(name = "p_price_discount")
    @ApiModelProperty(name = "product discounted price", required = true)
    private BigDecimal priceDiscount;

    @NotNull
    @Column(name = "p_valid_date")
    @ApiModelProperty(name = "product valid date", required = true)
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "Asia/Shanghai")
    private Date validDate;

    @NotNull
    @Column(name = "p_valid")
    @ApiModelProperty(name = "product valid flag", required = true)
    private Boolean valid;

    @NotNull
    @Column(name = "p_approve")
    @ApiModelProperty(name = "product approved flag", required = true)
    private Boolean approve;

    @NotNull
    @Column(name = "p_stock")
    private Integer stock;

    @NotNull
    @Column(name = "deleted")
    @ApiModelProperty(name = "product deleted flag", required = true)
    private Boolean deleted;

    public Long getId() {
        return id;
    }

    public void setId(Long pId) {
        this.id = pId;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getImage() {
        return image;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLocation() {
        return location;
    }

    public void setPriceOrigin(BigDecimal priceOrigin) {
        this.priceOrigin = priceOrigin;
    }

    public BigDecimal getPriceOrigin() {
        return priceOrigin;
    }

    public void setPriceDiscount(BigDecimal priceDiscount) {
        this.priceDiscount = priceDiscount;
    }

    public BigDecimal getPriceDiscount() {
        return priceDiscount;
    }

    @JsonFormat(pattern="yyyy-MM-dd")
    public void setValidDate(Date validDate) {
        this.validDate = validDate;
    }

    public Date getValidDate() {
        return validDate;
    }

    @JsonProperty(value = "valid")
    public void setValid(Boolean valid) {
        this.valid = valid;
    }

    @JsonProperty(value = "valid")
    @JsonIgnore
    public Boolean getValid() {
        return valid;
    }

    @JsonProperty(value = "approve")
    public void setApprove(Boolean approve) {
        this.approve = approve;
    }

    @JsonProperty(value = "approve")
    @JsonIgnore
    public Boolean getApprove() {
        return approve;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public Integer getStock() {
        return stock;
    }

    @JsonProperty(value = "deleted")
    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    @JsonProperty(value = "deleted")
    @JsonIgnore
    public Boolean getDeleted() {
        return deleted;
    }

    @Override
    public String toString() {
        return "Product{" +
                "userInfo.id=" + userInfo.getId() +
                ", title='" + title + '\'' +
                ", image='" + image + '\'' +
                ", location='" + location + '\'' +
                ", priceOrigin=" + priceOrigin +
                ", priceDiscount=" + priceDiscount +
                ", validDate=" + validDate +
                ", stock=" + stock +
                '}';
    }
}
