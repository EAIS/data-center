package cn.com.eais.entities;

import cn.com.eais.enums.UserType;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;
import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "user_info")
@ApiModel
@JsonInclude(NON_NULL)
public class UserInfo {

    @Id
    @Column(name = "u_id")
    @GeneratedValue(strategy = IDENTITY)
    @ApiModelProperty(hidden = true)
    private Long id;

    @NotNull
    @Column(name = "phone_number", unique = true, length = 11)
    @ApiModelProperty(value = "mobile phone number", required = true, example = "18988888888")
    private String phoneNumber;

    @Column(name = "u_account", unique = true, length = 50)
    @ApiModelProperty(value = "user account alias")
    private String account;

    @NotNull
    @Column(name = "password", length = 64)
    @ApiModelProperty(value = "user password", required = true)
    private String password;

    @Column(name = "u_name", unique = true)
    @ApiModelProperty(value = "user real name, using in identity")
    private String name;

    @Column(name = "u_identity", unique = true)
    @ApiModelProperty(value = "user identity, not check it reality for now")
    private String identity;

    @Column(name = "u_address")
    @ApiModelProperty(value = "user address")
    private String address;

    @NotNull
    @Column(name = "u_type", nullable = false, columnDefinition = "ENUM ('NORMAL', 'CUSTOMER')")
    @Enumerated(EnumType.STRING)
    @ApiModelProperty(value = "user type", required = true, example = "NORMAL/CUSTOMER")
    private UserType type;

    @NotNull
    @Column(name = "deleted", nullable = false)
    @ApiModelProperty(value = "user state, default to false", required = true, example = "false")
    private Boolean deleted;

    public UserInfo() {
    }

    public UserInfo(String phoneNumber, String account, String password, String name, String identity, String address, UserType type) {
        this.phoneNumber = phoneNumber;
        this.account = account;
        this.password = password;
        this.name = name;
        this.identity = identity;
        this.address = address;
        this.type = type;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    @JsonProperty("password")
    public void setPassword(String password) {
        this.password = password;
    }

    @JsonIgnore
    @JsonProperty("password")
    public String getPassword() {
        return password;
    }

    public void setType(UserType type) {
        this.type = type;
    }

    public UserType getType() {
        return type;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long uId) {
        this.id = uId;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIdentity() {
        return identity;
    }

    public void setIdentity(String identity) {
        this.identity = identity;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @JsonIgnore
    @JsonProperty("deleted")
    public Boolean getDeleted() {
        return deleted;
    }

    @JsonProperty("deleted")
    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    @Override
    public String toString() {
        return "UserInfo {" +
                "phoneNumber='" + phoneNumber + '\'' +
                ", account='" + account + '\'' +
                ", name='" + name + '\'' +
                ", identity='" + identity + '\'' +
                ", address='" + address + '\'' +
                ", type=" + type +
                '}';
    }
}
