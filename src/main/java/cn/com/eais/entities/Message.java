package cn.com.eais.entities;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "message")
public class Message {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "m_id")
    private Long id;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "u_receiver_id")
    private UserInfo receiverUserInfo;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "u_sender_id")
    private UserInfo senderUserInfo;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "p_id")
    private Product product;

    @NotNull
    @Column(name = "m_hasread")
    private Boolean hasRead;

    public void setReceiverUserInfo(UserInfo receiverUserInfo) {
        this.receiverUserInfo = receiverUserInfo;
    }

    public UserInfo getReceiverUserInfo() {
        return receiverUserInfo;
    }

    public void setSenderUserInfo(UserInfo senderUserInfo) {
        this.senderUserInfo = senderUserInfo;
    }

    public UserInfo getSenderUserInfo() {
        return senderUserInfo;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Product getProduct() {
        return product;
    }

    public void setHasRead(Boolean hasRead) {
        this.hasRead = hasRead;
    }

    public Boolean isHasRead() {
        return hasRead;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
