package cn.com.eais.model;

public class RequestHeaders {

    private final String xCorrelationId;
    private final String xClientName;
    private final String xClientVersion;
    private final String xClientKey;

    public RequestHeaders(String xCorrelationId, String xClientName, String xClientVersion, String xClientKey) {

        this.xCorrelationId = xCorrelationId;
        this.xClientName = xClientName;
        this.xClientVersion = xClientVersion;
        this.xClientKey = xClientKey;
    }

    public String getxCorrelationId() {
        return xCorrelationId;
    }

    public String getxClientName() {
        return xClientName;
    }

    public String getxClientVersion() {
        return xClientVersion;
    }

    public String getxClientKey() {
        return xClientKey;
    }
}
