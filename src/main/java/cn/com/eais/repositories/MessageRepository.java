package cn.com.eais.repositories;

import cn.com.eais.entities.Message;
import cn.com.eais.entities.UserInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface MessageRepository extends JpaRepository<Message, Long>{
    List<Message> findByReceiverUserInfo(UserInfo receiverUserInfo);

    List<Message> findBySenderUserInfo(UserInfo senderUserInfo);

    @Modifying(clearAutomatically = true)//清空缓存
    @Query(value = "UPDATE Message m SET m.hasRead = true WHERE m.id=?1")
    void updateById(Long id);

}
