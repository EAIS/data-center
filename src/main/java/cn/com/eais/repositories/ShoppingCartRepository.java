package cn.com.eais.repositories;

import cn.com.eais.entities.ShoppingCart;
import cn.com.eais.entities.UserInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ShoppingCartRepository extends JpaRepository<ShoppingCart, Long>{

    @Modifying(clearAutomatically = true)
    @Query(value = "UPDATE ShoppingCart s SET s.count = ?2 WHERE s.id = ?1")
    void updateCountById(Long id, int count);

    ShoppingCart findById(Long id);

    List<ShoppingCart> findByUserInfo(UserInfo userInfo);
}
