package cn.com.eais.repositories;

import cn.com.eais.entities.UserInfo;
import cn.com.eais.enums.UserType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserInfoRepository extends JpaRepository<UserInfo, Long> {
    UserInfo findByIdAndDeleted(Long id, Boolean deleted);

    UserInfo findByPhoneNumberAndDeleted(String phoneNumber, Boolean deleted);

    UserInfo findByAccountAndDeleted(String account, Boolean deleted);

    List<UserInfo> findByTypeAndDeleted(UserType type, Boolean deleted);

    List<UserInfo> findByDeleted(Boolean deleted);
}
