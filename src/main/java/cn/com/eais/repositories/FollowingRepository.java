package cn.com.eais.repositories;

import cn.com.eais.entities.Following;
import cn.com.eais.entities.UserInfo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface FollowingRepository extends JpaRepository<Following, Long>{
    List<Following> findByUserInfo(UserInfo userInfo);

    List<Following> findByFollowingUserInfo(UserInfo tempUserInfo);

    void deleteByUserInfoAndFollowingUserInfo(UserInfo userInfo, UserInfo followingUserInfo);
}
