package cn.com.eais.repositories;

import cn.com.eais.entities.Product;
import cn.com.eais.entities.UserInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public interface ProductRepository extends JpaRepository<Product, Long> {
    Product findByIdAndDeleted(Long pId, Boolean deleted);

    List<Product> findByUserInfoAndValidAndApproveAndDeleted(UserInfo userInfo, Boolean valid, Boolean approve, Boolean deleted);

    List<Product> findByTitleContainingAndValidAndApproveAndDeleted(String title, Boolean valid, Boolean approve, Boolean deleted);

    List<Product> findByLocationContainingAndValidAndApproveAndDeleted(String location, Boolean valid, Boolean approve, Boolean deleted);

    List<Product> findByPriceDiscountBetweenAndValidAndApproveAndDeleted(BigDecimal beginPrice, BigDecimal endPrice, Boolean valid, Boolean approve, Boolean deleted);

    List<Product> findByValidDateLessThanEqualAndValidAndApproveAndDeleted(Date pValidDate, Boolean valid, Boolean approve, Boolean deleted);

    List<Product> findByValidAndApproveAndDeleted(Boolean valid, Boolean approve, Boolean deleted);

    List<Product> findByApproveAndDeleted(Boolean approve, Boolean deleted);

    List<Product> findByDeleted(Boolean deleted);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query(value = "UPDATE Product p SET p.approve = true WHERE p.valid = true AND p.approve = false AND p.deleted = false")
    void updateApproveFlag();

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query(value = "UPDATE Product p SET p.valid = false WHERE date(p.validDate) < date(?1) AND p.valid = true AND p.approve = true")
    void updateValidFlag(Date validDate);

}


