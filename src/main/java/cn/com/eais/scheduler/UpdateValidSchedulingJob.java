package cn.com.eais.scheduler;

import cn.com.eais.services.ProductService;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.Marker;
import org.slf4j.helpers.BasicMarkerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class UpdateValidSchedulingJob {

    private ProductService productService;
    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());
    private final Marker MARKER = new BasicMarkerFactory().getMarker(this.getClass().getName());

    @Autowired
    public UpdateValidSchedulingJob(ProductService productService) {
        this.productService = productService;
    }

    @Scheduled(cron = "${cron.update.valid}")
    public void updateValidFlag() {
        final Date validDate = DateTime.now().toDate();
        LOGGER.info(MARKER, "Start scheduling job for update valid flag before date: {}", validDate);
        productService.updateValidFlag(validDate);
        LOGGER.info(MARKER, "End scheduling job for update valid flag");
    }
}
