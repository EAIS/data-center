package cn.com.eais.scheduler;

import cn.com.eais.services.ProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.Marker;
import org.slf4j.helpers.BasicMarkerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class UpdateApproveSchedulingJob {

    private ProductService productService;
    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());
    private final Marker MARKER = new BasicMarkerFactory().getMarker(this.getClass().getName());

    @Autowired
    public UpdateApproveSchedulingJob(ProductService productService) {
        this.productService = productService;
    }

    @Scheduled(cron = "${cron.update.approve}")
    public void updateApproveFlag() {
        LOGGER.info(MARKER, "Start scheduling job for update approve flag");
        productService.updateApproveFlag();
        LOGGER.info(MARKER, "End scheduling job for update approve flag");
    }
}
