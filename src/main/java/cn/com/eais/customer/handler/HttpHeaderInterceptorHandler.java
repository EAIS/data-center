package cn.com.eais.customer.handler;

import cn.com.eais.model.ErrorMessage;
import cn.com.eais.model.RequestHeaders;
import cn.com.eais.validator.ValidatorContext;
import cn.com.eais.validator.request.header.RequestHeaderValidatorFacade;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

import static cn.com.eais.contants.CorsProperties.*;

public class HttpHeaderInterceptorHandler extends HandlerInterceptorAdapter {


    private final int FORBIDDEN_STATUS = 403;
    private final String clientKey;

    public HttpHeaderInterceptorHandler(final String clientKey) {
        this.clientKey = clientKey;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String xCorrelationId = request.getHeader(X_CORRELATION_ID);
        String xClientName = request.getHeader(X_CLIENT_NAME);
        String xClientVersion = request.getHeader(X_CLIENT_VERSION);
        String xClientKey = request.getHeader(X_CLIENT_KEY);

        RequestHeaders headers = new RequestHeaders(xCorrelationId, xClientName, xClientVersion, xClientKey);
        ValidatorContext<RequestHeaders> validator = new RequestHeaderValidatorFacade(clientKey).requestHeaderValidator();
        List<ErrorMessage> errorMessages = validator.validate(headers);
        if (errorMessages.size() > 0) {
            response.setStatus(FORBIDDEN_STATUS);
            return false;
        }
        return true;
    }

}
