package cn.com.eais.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.List;

import static cn.com.eais.contants.CorsProperties.*;

@Configuration
@EnableSwagger2
@EnableWebMvc
public class SwaggerConfig {

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("cn.com.eais.controller"))
                .paths(PathSelectors.any())
                .build()
                .globalOperationParameters(globalOperationParameters());
    }

    private List<Parameter> globalOperationParameters() {
        List<Parameter> parameters = new ArrayList<>();
        parameters.add(createHeaderParameter(X_CORRELATION_ID));
        parameters.add(createHeaderParameter(X_CLIENT_NAME));
        parameters.add(createHeaderParameter(X_CLIENT_VERSION));
        parameters.add(createHeaderParameter(X_CLIENT_KEY));
        return parameters;
    }

    private Parameter createHeaderParameter(String name) {
        return new ParameterBuilder()
                .name(name)
                .parameterType("header")
                .modelRef(new ModelRef("string"))
                .required(true)
                .build();
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("EAIS data center")
                .description("This is data center api, managing all data belong to EAIS")
                .contact(new Contact("Mentu", "https://github.com/cqupt-gsy","gaosy.work@gmail.com"))
                .version("1.0")
                .build();
    }

}
