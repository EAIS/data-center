package cn.com.eais.config;


import cn.com.eais.customer.handler.HttpHeaderInterceptorHandler;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.config.annotation.*;

import java.util.List;

import static cn.com.eais.contants.CorsProperties.*;
import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.HttpMethod.OPTIONS;
import static org.springframework.http.HttpMethod.POST;

@EnableWebMvc
@Configuration
public class WebApplicationConfig extends WebMvcConfigurerAdapter {

    @Value("${security.key}")
    private String clientKey;

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/datas/**")
                .allowedOrigins("*")
                .allowedMethods(GET.name(), POST.name(), OPTIONS.name())
                .allowedHeaders(X_CORRELATION_ID, X_CLIENT_NAME, X_CLIENT_VERSION, X_CLIENT_KEY)
                .exposedHeaders(X_CORRELATION_ID, X_CLIENT_NAME, X_CLIENT_VERSION)
                .allowCredentials(false).maxAge(3600);
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new HttpHeaderInterceptorHandler(clientKey))
                .addPathPatterns("/datas/**", "/files/image/upload");
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("swagger-ui.html")
                .addResourceLocations("classpath:/META-INF/resources/");

        registry.addResourceHandler("/webjars/**")
                .addResourceLocations("classpath:/META-INF/resources/webjars/");
    }

    @Override
    public void configureHandlerExceptionResolvers(List<HandlerExceptionResolver> exceptionResolvers) {
        super.configureHandlerExceptionResolvers(exceptionResolvers);
    }
}
