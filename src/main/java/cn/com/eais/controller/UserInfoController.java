package cn.com.eais.controller;

import cn.com.eais.entities.UserInfo;
import cn.com.eais.model.ErrorMessage;
import cn.com.eais.services.UserInfoService;
import cn.com.eais.validator.ValidatorContext;
import cn.com.eais.validator.userinfo.UserInfoValidatorFacade;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.Marker;
import org.slf4j.helpers.BasicMarkerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static cn.com.eais.contants.CorsProperties.ACCEPT_APPLICATION_JSON_API_VALUE;
import static cn.com.eais.contants.CorsProperties.APPLICATION_JSON_API_VALUE;
import static org.springframework.http.HttpStatus.*;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
@RequestMapping(value = "/datas/userinfo")
public class UserInfoController {

    private UserInfoValidatorFacade userInfoValidatorFacade;
    private UserInfoService userInfoService;
    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());
    private final Marker MARKER = new BasicMarkerFactory().getMarker(this.getClass().getName());

    @Autowired
    public UserInfoController(UserInfoValidatorFacade userInfoValidatorFacade, UserInfoService userInfoService) {
        this.userInfoValidatorFacade = userInfoValidatorFacade;
        this.userInfoService = userInfoService;
    }

    @ApiOperation(value = "Registry userinfo api", notes = "Registry userinfo api")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Registry userinfo success", response = UserInfo.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Bad request parameters", response = ErrorMessage.class, responseContainer = "List"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @RequestMapping(value = "/registry", method = POST, headers = ACCEPT_APPLICATION_JSON_API_VALUE, produces = APPLICATION_JSON_API_VALUE)
    public ResponseEntity<?> registryUserInfo(@ApiParam @RequestBody UserInfo userInfo) {
        List<ErrorMessage> errorMessages = userInfoValidatorFacade.registryUserInfoValidator().validate(userInfo);
        if (errorMessages.size() > 0) {
            LOGGER.error(MARKER, "Validation error {}, for {}", errorMessages, userInfo);
            return new ResponseEntity<>(errorMessages, BAD_REQUEST);
        }

        final UserInfo savedUserInfo = userInfoService.addUserInfo(userInfo);
        LOGGER.info(MARKER, "Success create {}", savedUserInfo);
        return new ResponseEntity<>(savedUserInfo, OK);
    }

    @ApiOperation(value = "Login with phone number api", notes = "Login with phone number api")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Login success", response = UserInfo.class),
            @ApiResponse(code = 400, message = "Bad request parameters", response = ErrorMessage.class, responseContainer = "List"),
            @ApiResponse(code = 404, message = "Not found user"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @RequestMapping(value = "/loginPhone", method = POST, headers = ACCEPT_APPLICATION_JSON_API_VALUE, produces = APPLICATION_JSON_API_VALUE)
    public ResponseEntity<?> loginPhone(@ApiParam @RequestBody UserInfo userInfo) {
        List<ErrorMessage> errorMessages = userInfoValidatorFacade.loginPhoneUserInfoValidator().validate(userInfo);
        if (errorMessages.size() > 0) {
            LOGGER.error(MARKER, "Validation error {}, for {}", errorMessages, userInfo);
            return new ResponseEntity<>(errorMessages, BAD_REQUEST);
        }

        final UserInfo foundUserInfo = userInfoService.selectUserInfoByPhoneNumber(userInfo);
        if (foundUserInfo == null) {
            LOGGER.warn(MARKER, "Not found user {}", userInfo);
            return new ResponseEntity<>(NOT_FOUND);
        }

        LOGGER.info(MARKER, "Success login {}", foundUserInfo);
        return new ResponseEntity<>(foundUserInfo, OK);
    }

    @ApiOperation(value = "Login with account api", notes = "Login with account api")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Login success", response = UserInfo.class),
            @ApiResponse(code = 400, message = "Bad request parameters", response = ErrorMessage.class, responseContainer = "List"),
            @ApiResponse(code = 404, message = "Not found user"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @RequestMapping(value = "/loginAccount", method = POST, headers = ACCEPT_APPLICATION_JSON_API_VALUE, produces = APPLICATION_JSON_API_VALUE)
    public ResponseEntity<?> loginAccount(@ApiParam @RequestBody UserInfo userInfo) {
        List<ErrorMessage> errorMessages = userInfoValidatorFacade.loginAccountUserInfoValidator().validate(userInfo);
        if (errorMessages.size() > 0) {
            LOGGER.error(MARKER, "Validation error {}, for {}", errorMessages, userInfo);
            return new ResponseEntity<>(errorMessages, BAD_REQUEST);
        }

        final UserInfo foundUserInfo = userInfoService.selectUserInfoByAccount(userInfo);
        if (foundUserInfo == null) {
            LOGGER.warn(MARKER, "Not found user {}", userInfo);
            return new ResponseEntity<>(NOT_FOUND);
        }

        LOGGER.info(MARKER, "Success login {}", foundUserInfo);
        return new ResponseEntity<>(foundUserInfo, OK);
    }

    @ApiOperation(value = "Find user info by id", notes = "Find user info by id")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Found user info", response = UserInfo.class),
            @ApiResponse(code = 404, message = "No user info found"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @RequestMapping(value = "/search", method = GET, produces = APPLICATION_JSON_API_VALUE)
    public ResponseEntity<?> searchById(@ApiParam @RequestParam(name = "id") Long id) {
        final UserInfo foundUserInfo = userInfoService.findUserInfoById(id);

        if (foundUserInfo == null) {
            LOGGER.warn(MARKER, "Not found user for id {}", id);
            return new ResponseEntity<>(NOT_FOUND);
        }

        LOGGER.info(MARKER, "Found {}", foundUserInfo);
        return new ResponseEntity<>(foundUserInfo, OK);
    }

    @ApiOperation(value = "Update user details", notes = "Update user details")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Update success", response = UserInfo.class),
            @ApiResponse(code = 400, message = "Bad request parameters", response = ErrorMessage.class, responseContainer = "List"),
            @ApiResponse(code = 404, message = "No user found in data, update failed"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @RequestMapping(value = "/updateDetails", method = POST, headers = ACCEPT_APPLICATION_JSON_API_VALUE, produces = APPLICATION_JSON_API_VALUE)
    public ResponseEntity<?> updateUserDetails(@ApiParam @RequestBody UserInfo userInfo) {
        List<ErrorMessage> errorMessages = userInfoValidatorFacade.updateUserDetailsValidator().validate(userInfo);

        if (errorMessages.size() > 0) {
            LOGGER.error(MARKER, "Validation error {}, for {}", errorMessages, userInfo);
            return new ResponseEntity<>(errorMessages, BAD_REQUEST);
        }

        final UserInfo updatedUserInfo = userInfoService.updateUserDetails(userInfo);

        if (updatedUserInfo == null) {
            LOGGER.warn(MARKER, "Not found user {}", userInfo);
            return new ResponseEntity<>(NOT_FOUND);
        }

        LOGGER.info(MARKER, "Update success {}", updatedUserInfo);
        return new ResponseEntity<>(updatedUserInfo, OK);
    }

    @ApiOperation(value = "Update user phone", notes = "Update user phone")
    @ApiResponses({
            @ApiResponse(code = 204, message = "Update success"),
            @ApiResponse(code = 400, message = "Bad request parameters", response = ErrorMessage.class, responseContainer = "List"),
            @ApiResponse(code = 404, message = "No user found in data, update failed"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @RequestMapping(value = "/updatePhone", method = POST, headers = ACCEPT_APPLICATION_JSON_API_VALUE, produces = APPLICATION_JSON_API_VALUE)
    public ResponseEntity<?> updatePhoneNumber(@ApiParam @RequestBody UserInfo userInfo) {
        List<ErrorMessage> errorMessages = userInfoValidatorFacade.updatePhoneValidator().validate(userInfo);

        if (errorMessages.size() > 0) {
            LOGGER.error(MARKER, "Validation error {}, for {}", errorMessages, userInfo);
            return new ResponseEntity<>(errorMessages, BAD_REQUEST);
        }

        final UserInfo updatedUserInfo = userInfoService.updateUserPhone(userInfo);

        if (updatedUserInfo == null) {
            LOGGER.warn(MARKER, "Not found user {}", userInfo);
            return new ResponseEntity<>(NOT_FOUND);
        }

        LOGGER.info(MARKER, "Update success {}", updatedUserInfo);
        return new ResponseEntity<>(NO_CONTENT);
    }

    @ApiOperation(value = "Update user password", notes = "Update user password")
    @ApiResponses({
            @ApiResponse(code = 204, message = "Update success"),
            @ApiResponse(code = 400, message = "Bad request parameters", response = ErrorMessage.class, responseContainer = "List"),
            @ApiResponse(code = 404, message = "No user found in data, update failed"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @RequestMapping(value = "/updatePassword", method = POST, headers = ACCEPT_APPLICATION_JSON_API_VALUE, produces = APPLICATION_JSON_API_VALUE)
    public ResponseEntity<?> updatePassword(@ApiParam @RequestBody UserInfo[] userInfos) {
        final ValidatorContext<UserInfo> validator = userInfoValidatorFacade.updatePasswordValidator();
        List<ErrorMessage> errorMessages = validator.validate(userInfos[0]);
        errorMessages.addAll(validator.validate(userInfos[1]));
        if (errorMessages.size() > 0) {
            LOGGER.error(MARKER, "Validation error {}, for new user {}, and old user", errorMessages, userInfos[0], userInfos[1]);
            return new ResponseEntity<>(errorMessages, BAD_REQUEST);
        }

        final UserInfo updatedUserInfo = userInfoService.updateUserPassword(userInfos[0], userInfos[1]);

        if (updatedUserInfo == null) {
            LOGGER.warn(MARKER, "Not found user {}", userInfos[0]);
            return new ResponseEntity<>(NOT_FOUND);
        }

        LOGGER.info(MARKER, "Update success {}", updatedUserInfo);
        return new ResponseEntity<>(NO_CONTENT);
    }

    @ApiOperation(value = "Delete user temporary ", notes = "Delete user temporary")
    @ApiResponses({
            @ApiResponse(code = 204, message = "Delete success"),
            @ApiResponse(code = 404, message = "No user found in data, delete failed"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @RequestMapping(value = "/delete", method = POST, headers = ACCEPT_APPLICATION_JSON_API_VALUE, produces = APPLICATION_JSON_API_VALUE)
    public ResponseEntity<?> deleteUser(@ApiParam @RequestBody UserInfo userInfo) {
        final UserInfo foundUserInfo = userInfoService.deleteUserBy(userInfo.getId());

        if (foundUserInfo == null) {
            LOGGER.warn(MARKER, "Not found user for id {}", userInfo.getId());
            return new ResponseEntity<>(NOT_FOUND);
        }

        LOGGER.info(MARKER, "Delete success for id {}", userInfo.getId());
        return new ResponseEntity<>(NO_CONTENT);
    }
}