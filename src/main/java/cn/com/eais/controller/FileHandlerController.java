package cn.com.eais.controller;

import cn.com.eais.model.ErrorMessage;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;

import static cn.com.eais.contants.ErrorsProperties.EMPTY_ERROR;
import static cn.com.eais.contants.ErrorsProperties.ILLEGAL_ERROR;
import static cn.com.eais.contants.FileProperties.*;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
@RequestMapping(value = "/files/image")
public class FileHandlerController {

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    private String fileDir;

    @Autowired
    public FileHandlerController(@Value("${image.dir}") String fileDir) {
        this.fileDir = fileDir;
    }

    @ApiOperation(value = "Upload image file for product", notes = "Upload image file for product")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Upload image file success"),
            @ApiResponse(code = 400, message = "Upload image file failure with exception"),
            @ApiResponse(code = 500, message = "Internal exception"),

    })
    @RequestMapping(value = "/upload", method = POST)
    public ResponseEntity<?> uploadImage(@ApiParam @RequestParam("image") MultipartFile image) throws IOException {
        ErrorMessage errorMessage = validateImageFile(image);
        if (errorMessage != null) {
            LOGGER.error("Not support file with error: {}", errorMessage);
            return new ResponseEntity<>(errorMessage, BAD_REQUEST);
        }

        final String fileName = saveImageFile(image);
        LOGGER.info("Upload image file success with name: {}", fileName);
        return new ResponseEntity<>(fileName, OK);
    }

    @ApiOperation(value = "Show image for product", notes = "Show image for product")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Found image for product"),
            @ApiResponse(code = 500, message = "Not found product"),
    })
    @RequestMapping(value = "/{imageName:.+}", method = GET)
    public byte[] showImage(@ApiParam @PathVariable String imageName) throws IOException {
        final String pathName = fileDir + imageName;
        Path filePath = Paths.get(pathName);
        return Files.readAllBytes(filePath);
    }

    private ErrorMessage validateImageFile(MultipartFile image) {
        if (image == null) {
            return new ErrorMessage(IMAGE_FILE_FIELD, EMPTY_ERROR);
        }

        ErrorMessage errorMessages = null;
        final String fileExtension = MIMETYPE_TO_EXTENSION_MAPPER.get(image.getContentType());
        final long imageSize = image.getSize();
        if (isBlank(fileExtension) || imageSize > MAX_IMAGE_FILE_SIZE) {
            errorMessages = new ErrorMessage(IMAGE_FILE_FIELD, ILLEGAL_ERROR);
        }
        return errorMessages;
    }

    private String saveImageFile(MultipartFile image) throws IOException {
        final String fileExtension = MIMETYPE_TO_EXTENSION_MAPPER.get(image.getContentType());
        final String fileName = UUID.randomUUID().toString() + fileExtension;
        final String pathName = fileDir + fileName;
        createFileDirectoryIfNeeded();
        image.transferTo(new File(pathName));
        return fileName;
    }

    private void createFileDirectoryIfNeeded() {
        File directory = new File(fileDir);
        if (!directory.exists()) {
            directory.mkdirs();
        }
    }

}
