package cn.com.eais.controller;

import cn.com.eais.entities.Product;
import cn.com.eais.entities.UserInfo;
import cn.com.eais.model.ErrorMessage;
import cn.com.eais.services.ProductService;
import cn.com.eais.validator.product.ProductValidatorFacade;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.Marker;
import org.slf4j.helpers.BasicMarkerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import static cn.com.eais.contants.CorsProperties.ACCEPT_APPLICATION_JSON_API_VALUE;
import static cn.com.eais.contants.CorsProperties.APPLICATION_JSON_API_VALUE;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.springframework.http.HttpStatus.*;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
@RequestMapping(value = "/datas/product")
public class ProductController {

    private ProductService productService;
    private ProductValidatorFacade productValidatorFacade;
    private String host;
    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());
    private final Marker MARKER = new BasicMarkerFactory().getMarker(this.getClass().getName());

    @Autowired
    public ProductController(ProductService productService, ProductValidatorFacade productValidatorFacade, @Value("${image.host}") String host) {
        this.productService = productService;
        this.productValidatorFacade = productValidatorFacade;
        this.host = host;
    }

    @ApiOperation(value = "Add new product info", notes = "Add new product info")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Add product info success", response = Product.class),
            @ApiResponse(code = 400, message = "Validation error", response = ErrorMessage.class, responseContainer = "List"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @RequestMapping(value = "/addProduct", method = POST, headers = ACCEPT_APPLICATION_JSON_API_VALUE, produces = APPLICATION_JSON_API_VALUE)
    public ResponseEntity<?> addProduct(@ApiParam @RequestBody Product product) {
        List<ErrorMessage> errorMessages = productValidatorFacade.newProductValidator().validate(product);
        if (errorMessages.size() > 0) {
            LOGGER.error(MARKER, "Validation error {}, for {}", errorMessages, product);
            return new ResponseEntity<>(errorMessages, BAD_REQUEST);
        }

        Product savedProduct = productService.addNewProduct(product);
        constructImagePath(savedProduct);
        LOGGER.info(MARKER, "Success create {}", savedProduct);
        return new ResponseEntity<>(savedProduct, OK);
    }

    @ApiOperation(value = "Show valid products", notes = "Show valid products")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Found valid products", response = Product.class, responseContainer = "List"),
            @ApiResponse(code = 404, message = "Not found valid products"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @RequestMapping(value = "/products", method = GET, produces = APPLICATION_JSON_API_VALUE)
    public ResponseEntity<?> getProducts() {
        List<Product> products = productService.findProducts();
        if (products == null || products.size() == 0) {
            LOGGER.warn(MARKER, "No valid product info in database");
            return new ResponseEntity<>(NOT_FOUND);
        }

        LOGGER.info(MARKER, "Success found products with number: {}", products.size());
        products.forEach(this::constructImagePath);
        return new ResponseEntity<>(products, OK);
    }

    @ApiOperation(value = "Show valid products by user id", notes = "Show valid products by user id")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Found valid products", response = Product.class, responseContainer = "List"),
            @ApiResponse(code = 404, message = "Not found valid products"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @RequestMapping(value = "/userProducts", method = GET, produces = APPLICATION_JSON_API_VALUE)
    public ResponseEntity<?> getProductsByUserId(@ApiParam @RequestParam("id") UserInfo userInfo) {
        List<Product> products = productService.findProductsByUserId(userInfo);

        if (products == null || products.size() == 0) {
            LOGGER.warn(MARKER, "No valid product info in database");
            return new ResponseEntity<>(NOT_FOUND);
        }

        LOGGER.info(MARKER, "Success found products with number: {}", products.size());
        products.forEach(this::constructImagePath);
        return new ResponseEntity<>(products, OK);
    }

    @ApiOperation(value = "Show valid products by title", notes = "Show valid products by title")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Found valid products", response = Product.class, responseContainer = "List"),
            @ApiResponse(code = 404, message = "Not found valid products"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @RequestMapping(value = "/searchByTitle", method = GET, produces = APPLICATION_JSON_API_VALUE)
    public ResponseEntity<?> getProductsByTitle(@ApiParam @RequestParam("title") String title) {
        List<Product> products = productService.findProductsByTitle(title);

        if (products == null || products.size() == 0) {
            LOGGER.warn(MARKER, "No valid product info in database");
            return new ResponseEntity<>(NOT_FOUND);
        }

        LOGGER.info(MARKER, "Success found products with number: {}", products.size());
        products.forEach(this::constructImagePath);
        return new ResponseEntity<>(products, OK);
    }

    @ApiOperation(value = "Show valid products by location", notes = "Show valid products by location")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Found valid products", response = Product.class, responseContainer = "List"),
            @ApiResponse(code = 404, message = "Not found valid products"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @RequestMapping(value = "/searchByLocation", method = GET, produces = APPLICATION_JSON_API_VALUE)
    public ResponseEntity<?> getProductsByLocation(@ApiParam @RequestParam("location") String location) {
        List<Product> products = productService.findProductsByLocation(location);

        if (products == null || products.size() == 0) {
            LOGGER.warn(MARKER, "No valid product info in database");
            return new ResponseEntity<>(NOT_FOUND);
        }

        LOGGER.info(MARKER, "Success found products with number: {}", products.size());
        products.forEach(this::constructImagePath);
        return new ResponseEntity<>(products, OK);
    }

    @ApiOperation(value = "Show valid products by prices", notes = "Show valid products by prices")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Found valid products", response = Product.class, responseContainer = "List"),
            @ApiResponse(code = 404, message = "Not found valid products"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @RequestMapping(value = "/searchByPrice", method = GET, produces = APPLICATION_JSON_API_VALUE)
    public ResponseEntity<?> getProductsByPrices(@ApiParam @RequestParam("beginPrice") BigDecimal beginPrice,
                                                 @ApiParam @RequestParam("endPrice") BigDecimal endPrice) {
        List<Product> products = productService.findProductsByPrices(beginPrice, endPrice);

        if (products == null || products.size() == 0) {
            LOGGER.warn(MARKER, "No valid product info in database");
            return new ResponseEntity<>(NOT_FOUND);
        }

        LOGGER.info(MARKER, "Success found products with number: {}", products.size());
        products.forEach(this::constructImagePath);
        return new ResponseEntity<>(products, OK);
    }

    @ApiOperation(value = "Show valid products by valid date", notes = "Show valid products by valid date")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Found valid products", response = Product.class, responseContainer = "List"),
            @ApiResponse(code = 404, message = "Not found valid products"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @RequestMapping(value = "/searchByDate", method = GET, produces = APPLICATION_JSON_API_VALUE)
    public ResponseEntity<?> getProductsByValidDate(@ApiParam @RequestParam("validDate") @DateTimeFormat(pattern="yyyy-MM-dd") Date validDate) {
        List<Product> products = productService.findProductsByDate(validDate);

        if (products == null || products.size() == 0) {
            LOGGER.warn(MARKER, "No valid product info in database");
            return new ResponseEntity<>(NOT_FOUND);
        }

        LOGGER.info(MARKER, "Success found products with number: {}", products.size());
        products.forEach(this::constructImagePath);
        return new ResponseEntity<>(products, OK);
    }

    @ApiOperation(value = "Update product details", notes = "Update product details")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Update success", response = Product.class),
            @ApiResponse(code = 400, message = "Bad request parameters", response = ErrorMessage.class, responseContainer = "List"),
            @ApiResponse(code = 404, message = "No product found in data, update failed"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @RequestMapping(value = "/update", method = POST, headers = ACCEPT_APPLICATION_JSON_API_VALUE, produces = APPLICATION_JSON_API_VALUE)
    public ResponseEntity<?> updateProduct(@ApiParam @RequestBody Product product) {
        List<ErrorMessage> errorMessages = productValidatorFacade.newProductValidator().validate(product);
        if (errorMessages.size() > 0) {
            LOGGER.error(MARKER, "Validation error {}, for {}", errorMessages, product);
            return new ResponseEntity<>(errorMessages, BAD_REQUEST);
        }

        Product savedProduct = productService.updateProduct(product);
        if (savedProduct == null) {
            LOGGER.warn(MARKER, "Not found product info {}", product);
            return new ResponseEntity<>(NOT_FOUND);
        }

        constructImagePath(savedProduct);
        LOGGER.info(MARKER, "Success update {}", savedProduct);
        return new ResponseEntity<>(savedProduct, OK);
    }

    @ApiOperation(value = "Delete product temporary ", notes = "Delete product temporary")
    @ApiResponses({
            @ApiResponse(code = 204, message = "Delete success"),
            @ApiResponse(code = 404, message = "No product found in data, delete failed"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @RequestMapping(value = "/delete", method = POST, headers = ACCEPT_APPLICATION_JSON_API_VALUE, produces = APPLICATION_JSON_API_VALUE)
    public ResponseEntity<?> deleteProduct(@ApiParam @RequestBody Product product) {
        final Product foundProduct = productService.deleteProductById(product.getId());

        if (foundProduct == null) {
            LOGGER.warn(MARKER, "Not found product for id {}", product.getId());
            return new ResponseEntity<>(NOT_FOUND);
        }

        LOGGER.info(MARKER, "Delete success for id {}", product.getId());
        return new ResponseEntity<>(NO_CONTENT);
    }

    private void constructImagePath(Product product) {
        if (!isBlank(product.getImage())) {
            product.setImage(host + product.getImage());
        }
    }
}
