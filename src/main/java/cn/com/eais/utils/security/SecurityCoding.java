package cn.com.eais.utils.security;

import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;

public class SecurityCoding {

    private static StandardPBEStringEncryptor standardPBEStringEncryptor;

    static {
        standardPBEStringEncryptor = new StandardPBEStringEncryptor();
        standardPBEStringEncryptor.setAlgorithm("PBEWithMD5AndDES");
        standardPBEStringEncryptor.setPassword("d-%fjka21j-3249cmQWEa");
    }

    public static String encrypt(String encodeString) {
        return standardPBEStringEncryptor.encrypt(encodeString);
    }

    public static String decrypt(String decodeString) {
        return standardPBEStringEncryptor.decrypt(decodeString);
    }
}
