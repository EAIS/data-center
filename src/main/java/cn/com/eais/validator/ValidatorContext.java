package cn.com.eais.validator;

import cn.com.eais.model.ErrorMessage;

import java.util.ArrayList;
import java.util.List;

public class ValidatorContext<T> {
    private List<Validator> validators = new ArrayList<>();

    public void addValidator(Validator validator) {
        validators.add(validator);
    }

    @SuppressWarnings("unchecked")
    public List<ErrorMessage> validate(T model) {
        List<ErrorMessage> errorMessages = new ArrayList<>();
        validators.forEach(validator -> errorMessages.addAll(validator.validate(model)));
        return errorMessages;
    }
}
