package cn.com.eais.validator.product.components;

import cn.com.eais.contants.ProductProperties;
import cn.com.eais.entities.Product;
import cn.com.eais.model.ErrorMessage;
import cn.com.eais.validator.Validator;

import java.util.ArrayList;
import java.util.List;

import static cn.com.eais.validator.commons.ValidatorUtils.EMPTY_VALIDATOR;

public class StockValidator implements Validator<Product> {

    @Override
    public List<ErrorMessage> validate(Product model) {
        List<ErrorMessage> errorMessages = new ArrayList<>();
        EMPTY_VALIDATOR(model.getStock(), ProductProperties.STOCK_FIELD, errorMessages);
        return errorMessages;
    }
}
