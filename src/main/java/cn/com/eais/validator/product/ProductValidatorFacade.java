package cn.com.eais.validator.product;

import cn.com.eais.entities.Product;
import cn.com.eais.validator.ValidatorContext;
import cn.com.eais.validator.product.components.*;
import org.springframework.stereotype.Component;

@Component
public class ProductValidatorFacade {

    public ValidatorContext<Product> newProductValidator() {
        ValidatorContext<Product> validatorContext = new ValidatorContext<>();
        validatorContext.addValidator(new UserIdValidator());
        validatorContext.addValidator(new TitleValidator());
        validatorContext.addValidator(new LocationValidator());
        validatorContext.addValidator(new PriceValidator());
        validatorContext.addValidator(new DateValidator());
        validatorContext.addValidator(new StockValidator());
        return validatorContext;
    }
}
