package cn.com.eais.validator.product.components;

import cn.com.eais.entities.Product;
import cn.com.eais.model.ErrorMessage;
import cn.com.eais.validator.Validator;

import java.util.ArrayList;
import java.util.List;

import static cn.com.eais.contants.ProductProperties.LOCATION_FIELD;
import static cn.com.eais.validator.commons.ValidatorUtils.EMPTY_VALIDATOR;
import static cn.com.eais.validator.commons.ValidatorUtils.MAX_LENGTH_VALIDATOR;

public class LocationValidator implements Validator<Product> {

    private static final int MAX_LOCATION_LENGTH = 100;

    @Override
    public List<ErrorMessage> validate(Product model) {
        List<ErrorMessage> errorMessages = new ArrayList<>();
        EMPTY_VALIDATOR(model.getLocation(), LOCATION_FIELD, errorMessages);
        MAX_LENGTH_VALIDATOR(model.getLocation(), LOCATION_FIELD, errorMessages, MAX_LOCATION_LENGTH);
        return errorMessages;
    }
}
