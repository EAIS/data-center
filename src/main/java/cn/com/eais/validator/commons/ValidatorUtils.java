package cn.com.eais.validator.commons;

import cn.com.eais.model.ErrorMessage;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

import static cn.com.eais.contants.ErrorsProperties.EMPTY_ERROR;
import static cn.com.eais.contants.ErrorsProperties.ILLEGAL_ERROR;
import static org.apache.commons.lang3.StringUtils.isBlank;

public final class ValidatorUtils {

    public static void EMPTY_VALIDATOR(String field, String fieldName, List<ErrorMessage> errors) {
        if (isBlank(field)) {
            errors.add(new ErrorMessage(fieldName, EMPTY_ERROR));
        }
    }

    public static void EMPTY_VALIDATOR(Long field, String fieldName, List<ErrorMessage> errors) {
        if (field == null) {
            errors.add(new ErrorMessage(fieldName, EMPTY_ERROR));
        }
    }

    public static void EMPTY_VALIDATOR(Integer field, String fieldName, List<ErrorMessage> errors) {
        if (field == null) {
            errors.add(new ErrorMessage(fieldName, EMPTY_ERROR));
        }
    }

    public static void EMPTY_VALIDATOR(BigDecimal field, String fieldName, List<ErrorMessage> errors) {
        if (field == null) {
            errors.add(new ErrorMessage(fieldName, EMPTY_ERROR));
        }
    }

    public static void EMPTY_VALIDATOR(Date field, String fieldName, List<ErrorMessage> errors) {
        if (field == null) {
            errors.add(new ErrorMessage(fieldName, EMPTY_ERROR));
        }
    }

    public static void MAX_LENGTH_VALIDATOR(String field, String fieldName, List<ErrorMessage> errors, int maxLength) {
        if (!isBlank(field) && field.length() > maxLength) {
            errors.add(new ErrorMessage(fieldName, ILLEGAL_ERROR));
        }
    }

    public static void REGEX_VALIDATOR(String field, String fieldName, List<ErrorMessage> errors, String regex) {
        Pattern pattern = Pattern.compile(regex);
        if (!isBlank(field) && !pattern.matcher(field).matches()) {
            errors.add(new ErrorMessage(fieldName, ILLEGAL_ERROR));
        }
    }

    private ValidatorUtils() {
    }
}
