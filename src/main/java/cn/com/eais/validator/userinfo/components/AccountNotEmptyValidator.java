package cn.com.eais.validator.userinfo.components;

import cn.com.eais.entities.UserInfo;
import cn.com.eais.model.ErrorMessage;
import cn.com.eais.validator.Validator;

import java.util.ArrayList;
import java.util.List;

import static cn.com.eais.contants.UserInfoProperties.ACCOUNT_FIELD;
import static cn.com.eais.validator.commons.ValidatorUtils.EMPTY_VALIDATOR;

public class AccountNotEmptyValidator implements Validator<UserInfo> {

    @Override
    public List<ErrorMessage> validate(UserInfo model) {
        List<ErrorMessage> errors = new ArrayList<>();
        EMPTY_VALIDATOR(model.getAccount(), ACCOUNT_FIELD, errors);
        return errors;
    }
}
