package cn.com.eais.validator.userinfo;

import cn.com.eais.entities.UserInfo;
import cn.com.eais.validator.ValidatorContext;
import cn.com.eais.validator.userinfo.components.*;
import org.springframework.stereotype.Component;

@Component
public class UserInfoValidatorFacade {

    public ValidatorContext<UserInfo> registryUserInfoValidator() {
        ValidatorContext<UserInfo> validatorContext = new ValidatorContext<>();
        validatorContext.addValidator(new PhoneNumberValidator());
        validatorContext.addValidator(new AccountValidator());
        validatorContext.addValidator(new PasswordValidator());
        validatorContext.addValidator(new NameValidator());
        validatorContext.addValidator(new IdentityValidator());
        validatorContext.addValidator(new AddressValidator());
        validatorContext.addValidator(new UserTypeValidator());
        return validatorContext;
    }

    public ValidatorContext<UserInfo> loginPhoneUserInfoValidator() {
        ValidatorContext<UserInfo> validatorContext = new ValidatorContext<>();
        validatorContext.addValidator(new PhoneNumberValidator());
        validatorContext.addValidator(new PasswordValidator());
        return validatorContext;
    }

    public ValidatorContext<UserInfo> loginAccountUserInfoValidator() {
        ValidatorContext<UserInfo> validatorContext = new ValidatorContext<>();
        validatorContext.addValidator(new AccountNotEmptyValidator());
        validatorContext.addValidator(new AccountValidator());
        validatorContext.addValidator(new PasswordValidator());
        return validatorContext;
    }

    public ValidatorContext<UserInfo> updateUserDetailsValidator() {
        ValidatorContext<UserInfo> validatorContext = new ValidatorContext<>();
        validatorContext.addValidator(new AccountValidator());
        validatorContext.addValidator(new NameValidator());
        validatorContext.addValidator(new IdentityValidator());
        validatorContext.addValidator(new AddressValidator());
        validatorContext.addValidator(new UserTypeValidator());
        return validatorContext;
    }

    public ValidatorContext<UserInfo> updatePhoneValidator() {
        ValidatorContext<UserInfo> validatorContext = new ValidatorContext<>();
        validatorContext.addValidator(new PhoneNumberValidator());
        return validatorContext;
    }

    public ValidatorContext<UserInfo> updatePasswordValidator() {
        ValidatorContext<UserInfo> validatorContext = new ValidatorContext<>();
        validatorContext.addValidator(new PasswordValidator());
        return validatorContext;
    }
}
