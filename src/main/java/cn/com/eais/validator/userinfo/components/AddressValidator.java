package cn.com.eais.validator.userinfo.components;

import cn.com.eais.entities.UserInfo;
import cn.com.eais.model.ErrorMessage;
import cn.com.eais.validator.Validator;

import java.util.ArrayList;
import java.util.List;

import static cn.com.eais.contants.UserInfoProperties.ADDRESS_FIELD;
import static cn.com.eais.validator.commons.ValidatorUtils.MAX_LENGTH_VALIDATOR;

public class AddressValidator implements Validator<UserInfo> {

    private static final int MAX_ADDRESS_LENGTH = 100;

    @Override
    public List<ErrorMessage> validate(UserInfo model) {
        List<ErrorMessage> errors = new ArrayList<>();
        MAX_LENGTH_VALIDATOR(model.getAddress(), ADDRESS_FIELD, errors, MAX_ADDRESS_LENGTH);
        return errors;
    }
}
