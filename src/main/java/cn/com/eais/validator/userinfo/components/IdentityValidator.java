package cn.com.eais.validator.userinfo.components;

import cn.com.eais.entities.UserInfo;
import cn.com.eais.model.ErrorMessage;
import cn.com.eais.validator.Validator;

import java.util.ArrayList;
import java.util.List;

import static cn.com.eais.contants.UserInfoProperties.IDENTITY_FIELD;
import static cn.com.eais.validator.commons.ValidatorUtils.REGEX_VALIDATOR;

public class IdentityValidator implements Validator<UserInfo> {

    private static final String IDENTITY_RULE = "(^\\d{15}$)|(^\\d{18}$)|(^\\d{17}(\\d|X|x)$)";


    @Override
    public List<ErrorMessage> validate(UserInfo model) {
        List<ErrorMessage> errors = new ArrayList<>();
        REGEX_VALIDATOR(model.getIdentity(), IDENTITY_FIELD, errors, IDENTITY_RULE);
        return errors;
    }
}
