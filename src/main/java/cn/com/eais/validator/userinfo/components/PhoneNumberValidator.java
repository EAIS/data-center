package cn.com.eais.validator.userinfo.components;

import cn.com.eais.entities.UserInfo;
import cn.com.eais.model.ErrorMessage;
import cn.com.eais.validator.Validator;

import java.util.ArrayList;
import java.util.List;

import static cn.com.eais.contants.UserInfoProperties.PHONE_NUMBER_FIELD;
import static cn.com.eais.validator.commons.ValidatorUtils.EMPTY_VALIDATOR;
import static cn.com.eais.validator.commons.ValidatorUtils.REGEX_VALIDATOR;

public class PhoneNumberValidator implements Validator<UserInfo> {

    private static final String PHONE_NUMBER_RULE = "^1(3[0-9]|4[57]|5[0-35-9]|7[0135678]|8[0-9])\\d{8}$";


    @Override
    public List<ErrorMessage> validate(UserInfo model) {
        List<ErrorMessage> errors = new ArrayList<>();
        EMPTY_VALIDATOR(model.getPhoneNumber(), PHONE_NUMBER_FIELD, errors);
        REGEX_VALIDATOR(model.getPhoneNumber(), PHONE_NUMBER_FIELD, errors, PHONE_NUMBER_RULE);
        return errors;
    }
}
