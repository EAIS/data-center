package cn.com.eais.validator.userinfo.components;

import cn.com.eais.entities.UserInfo;
import cn.com.eais.model.ErrorMessage;
import cn.com.eais.validator.Validator;

import java.util.ArrayList;
import java.util.List;

import static cn.com.eais.contants.ErrorsProperties.EMPTY_ERROR;
import static cn.com.eais.contants.UserInfoProperties.TYPE_FIELD;

public class UserTypeValidator implements Validator<UserInfo> {


    @Override
    public List<ErrorMessage> validate(UserInfo model) {
        List<ErrorMessage> errors = new ArrayList<>();
        if (model.getType() == null) {
            errors.add(new ErrorMessage(TYPE_FIELD, EMPTY_ERROR));
        }
        return errors;
    }
}
