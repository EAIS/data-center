package cn.com.eais.validator.userinfo.components;

import cn.com.eais.entities.UserInfo;
import cn.com.eais.model.ErrorMessage;
import cn.com.eais.validator.Validator;

import java.util.ArrayList;
import java.util.List;

import static cn.com.eais.contants.UserInfoProperties.PASSWORD_FIELD;
import static cn.com.eais.validator.commons.ValidatorUtils.EMPTY_VALIDATOR;
import static cn.com.eais.validator.commons.ValidatorUtils.MAX_LENGTH_VALIDATOR;

public class PasswordValidator implements Validator<UserInfo> {

    private static final int MAX_PASSWORD_LENGTH = 64;

    @Override
    public List<ErrorMessage> validate(UserInfo model) {
        List<ErrorMessage> errors = new ArrayList<>();
        EMPTY_VALIDATOR(model.getPassword(), PASSWORD_FIELD, errors);
        MAX_LENGTH_VALIDATOR(model.getPassword(), PASSWORD_FIELD, errors, MAX_PASSWORD_LENGTH);
        return errors;
    }
}
