package cn.com.eais.validator;

import cn.com.eais.model.ErrorMessage;

import java.util.List;

public interface Validator<T> {
    List<ErrorMessage> validate(T model);
}
