package cn.com.eais.validator.request.header.components;

import cn.com.eais.model.ErrorMessage;
import cn.com.eais.model.RequestHeaders;
import cn.com.eais.validator.Validator;

import java.util.ArrayList;
import java.util.List;

import static cn.com.eais.contants.CorsProperties.X_CLIENT_VERSION;
import static cn.com.eais.contants.CorsProperties.X_CLIENT_VERSION_EMPTY_ERROR;
import static org.apache.commons.lang3.StringUtils.isBlank;

public class ClientVersionValidator implements Validator<RequestHeaders> {

    @Override
    public List<ErrorMessage> validate(RequestHeaders model) {
        List<ErrorMessage> errorMessages = new ArrayList<>();
        if (isBlank(model.getxClientVersion())) {
            errorMessages.add(new ErrorMessage(X_CLIENT_VERSION, X_CLIENT_VERSION_EMPTY_ERROR));
        }
        return errorMessages;
    }
}
