package cn.com.eais.validator.request.header.components;

import cn.com.eais.enums.ClientName;
import cn.com.eais.model.ErrorMessage;
import cn.com.eais.model.RequestHeaders;
import cn.com.eais.validator.Validator;

import java.util.ArrayList;
import java.util.List;

import static cn.com.eais.contants.CorsProperties.X_CLIENT_NAME;
import static cn.com.eais.contants.CorsProperties.X_CLIENT_NAME_EMPTY_ERROR;
import static cn.com.eais.contants.CorsProperties.X_CLIENT_NAME_MATCH_ERROR;
import static org.apache.commons.lang3.StringUtils.isBlank;

public class ClientNameValidator implements Validator<RequestHeaders> {

    @Override
    public List<ErrorMessage> validate(RequestHeaders model) {
        List<ErrorMessage> errorMessages = new ArrayList<>();
        if (isBlank(model.getxClientName())) {
            errorMessages.add(new ErrorMessage(X_CLIENT_NAME, X_CLIENT_NAME_EMPTY_ERROR));
        } else if (!ClientName.getClientNames().contains(model.getxClientName())) {
            errorMessages.add(new ErrorMessage(X_CLIENT_NAME, X_CLIENT_NAME_MATCH_ERROR));
        }
        return errorMessages;
    }
}
