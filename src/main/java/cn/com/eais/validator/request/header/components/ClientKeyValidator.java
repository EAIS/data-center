package cn.com.eais.validator.request.header.components;

import cn.com.eais.model.ErrorMessage;
import cn.com.eais.model.RequestHeaders;
import cn.com.eais.validator.Validator;

import java.util.ArrayList;
import java.util.List;

import static cn.com.eais.contants.CorsProperties.X_CLIENT_KEY;
import static cn.com.eais.contants.CorsProperties.X_CLIENT_KEY_EMPTY_ERROR;
import static cn.com.eais.contants.CorsProperties.X_CLIENT_KEY_MATCH_ERROR;
import static org.apache.commons.lang3.StringUtils.isBlank;

public class ClientKeyValidator implements Validator<RequestHeaders> {

    private final String clientKey;

    public ClientKeyValidator(String clientKey) {
        this.clientKey = clientKey;
    }

    @Override
    public List<ErrorMessage> validate(RequestHeaders model) {
        List<ErrorMessage> errorMessages = new ArrayList<>();
        if (isBlank(model.getxClientKey())) {
            errorMessages.add(new ErrorMessage(X_CLIENT_KEY, X_CLIENT_KEY_EMPTY_ERROR));
        } else if (!this.clientKey.equals(model.getxClientKey())) {
            errorMessages.add(new ErrorMessage(X_CLIENT_KEY, X_CLIENT_KEY_MATCH_ERROR));
        }
        return errorMessages;
    }

}
