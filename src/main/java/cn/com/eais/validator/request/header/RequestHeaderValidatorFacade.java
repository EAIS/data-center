package cn.com.eais.validator.request.header;

import cn.com.eais.model.RequestHeaders;
import cn.com.eais.validator.ValidatorContext;
import cn.com.eais.validator.request.header.components.ClientKeyValidator;
import cn.com.eais.validator.request.header.components.ClientNameValidator;
import cn.com.eais.validator.request.header.components.ClientVersionValidator;
import cn.com.eais.validator.request.header.components.CorrelationIdValidator;

public class RequestHeaderValidatorFacade {


    private final String clientKey;

    public RequestHeaderValidatorFacade(final String clientKey) {
        this.clientKey = clientKey;
    }

    public ValidatorContext<RequestHeaders> requestHeaderValidator() {
        ValidatorContext<RequestHeaders> validatorContext = new ValidatorContext<>();
        validatorContext.addValidator(new CorrelationIdValidator());
        validatorContext.addValidator(new ClientNameValidator());
        validatorContext.addValidator(new ClientVersionValidator());
        validatorContext.addValidator(new ClientKeyValidator(clientKey));
        return validatorContext;
    }
}
