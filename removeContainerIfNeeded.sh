#!/bin/bash

containerName=$1

matchedContainerID=$(docker ps --filter="name=$containerName" -q | xargs)
[[ -n $matchedContainerID ]] && docker stop $matchedContainerID

matchedContainerID=$(docker ps -a --filter="name=$containerName" -q | xargs)
[[ -n $matchedContainerID ]] && docker rm $matchedContainerID

exit 0